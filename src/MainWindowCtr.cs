﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using System.Media;
using System.Diagnostics;

namespace WFApp
{
    public delegate void DelegateActKey(string data);
    public delegate void DelegateMacroData(byte type, string str, int value, int index, bool fc);

    public partial class MainWindowCtr : Form
    {
        private static string StatusStripMessage = "wxMacro v0.7.1rc by XgenDZ";

        // for saving real array index (for compliance)
        private static List<int> macroCollection_indexList = new List<int>();
        private static List<int> macroEnabled_indexList = new List<int>();

        public static int insertMode = 0; // 0 - A; 1 - B
        public static int macroData_selectedIndex;
        private static Stopwatch eventTimer = new Stopwatch();
        private static List<int> keyDownBlockList = new List<int>();

        private Form fmKeySelect;

        // ------ Main ------
        public MainWindowCtr()
        {
            InitializeComponent();

            macroCollection_Update();
            group_macroConfig_UpdateState();
            statusStrip_mainMessage.Text = StatusStripMessage;

            fDisableMainMouseBtn.Checked = KeyHandler.ignoreMouseButtonsLR;
            fUseVirtualKeyCodes.Checked = KeyHandler.useVirtualKeyCodes;
            f_mwTimeout.Value = KeyHandler.mw_timeout;
        }

        private void ResetFocus()
        {
            fMacroActivationKey.Focus();
        }

        private void macroCollection_Update()
        {
            macroCollection.Items.Clear();
            macroCollection_indexList.Clear();
            if (Program.mainMacroCollection.Count == 0) return;

            for (int i = 0; i < Program.mainMacroCollection.Count; i++)
                if (Program.mainMacroCollection[i].enabled == false)
                {
                    string str = Program.mainMacroCollection[i].Name;
                    if (i == Program.selectedMacroIndex) str += " *";
                    macroCollection.Items.Add(str);
                    macroCollection_indexList.Add(i);
                }
        }

        private void macroEnabled_Update()
        {
            macroEnabled.Items.Clear();
            macroEnabled_indexList.Clear();
            if (Program.mainMacroCollection.Count == 0) return;

            for (int i = 0; i < Program.mainMacroCollection.Count; i++)
            {
                if (Program.mainMacroCollection[i].enabled == true)
                {
                    macroEnabled.Items.Add(Program.mainMacroCollection[i].Name);
                    macroEnabled_indexList.Add(i);
                }
            }
        }

        private void macroData_mUpdate()
        {
            macroData.Items.Clear();
            if (Program.selectedMacroIndex == -1) return; // no selection

            string str = ""; int index = Program.selectedMacroIndex;
            for (int i = 0; i < Program.mainMacroCollection[index].data.Count; i++)
            {
                if (Program.mainMacroCollection[index].data[i].Type == Macro.Event.Delay)
                    str = "--" + "     " + (int)(Program.mainMacroCollection[index].data[i].DelayValue *
                        Program.mainMacroCollection[index].delayMultiplier) + " ms";
                else if (Program.mainMacroCollection[index].data[i].Type == Macro.Event.KeyDown)
                    str = "\\/" + "     " + Program.mainMacroCollection[index].data[i].key.str;
                else if (Program.mainMacroCollection[index].data[i].Type == Macro.Event.KeyUp)
                    str = "/\\" + "     " + Program.mainMacroCollection[index].data[i].key.str;

                if (i == macroData_selectedIndex) str += " *";

                macroData.Items.Add(str);
            }
        }

        private void macroData_AddItem(Macro.Event ev)
        {
            string str = "";

            if (ev.Type == Macro.Event.Delay) str = "--" + "     " + (int)(ev.DelayValue) + " ms";
            else if (ev.Type == Macro.Event.KeyDown) str = "\\/" + "     " + ev.key.str;
            else if (ev.Type == Macro.Event.KeyUp) str = "/\\" + "     " + ev.key.str;
            //str += " *";

            macroData.Items.Add(str);
        }

        private void macroData_AddItem(Macro.Event ev, int index)
        {
            string str = "";

            if (ev.Type == Macro.Event.Delay) str = "--" + "     " + (int)(ev.DelayValue) + " ms";
            else if (ev.Type == Macro.Event.KeyDown) str = "\\/" + "     " + ev.key.str;
            else if (ev.Type == Macro.Event.KeyUp) str = "/\\" + "     " + ev.key.str;
            //str += " *";

            macroData.Items.Insert(index, str);
        }

        private void group_macroConfig_UpdateState()
        {
            if (MainWindowCtr.macroCollection_indexList.Count == 0 ||
                Program.mainMacroCollection.Count == 0 ||
                Program.selectedMacroIndex == -1 )
            {
                group_macroConfig.Enabled = false;
                bDeleteMacro.Enabled = false;
            }
            else if (group_macroConfig.Enabled == false)
            {
                group_macroConfig.Enabled = true;
                bDeleteMacro.Enabled = true;
            }
        }

        private void group_macroConfig_FillData()
        {
            if (macroCollection.SelectedIndex == -1) return; // no selection

            fMacroName.Text = Program.mainMacroCollection[Program.selectedMacroIndex].Name;
            fMacroActivationKey.Text = Program.mainMacroCollection[Program.selectedMacroIndex].actKey.str;
            fActivationMethod.SelectedIndex = Program.mainMacroCollection[Program.selectedMacroIndex].ActMethod;
            fMacroRepeat.Checked = Program.mainMacroCollection[Program.selectedMacroIndex].repeat;
            fPreventTermination.Checked = Program.mainMacroCollection[Program.selectedMacroIndex].preventTermination;
            fEventDelayMultiplier.Value = (decimal)Program.mainMacroCollection[Program.selectedMacroIndex].delayMultiplier;
            fAddEventSel.SelectedIndex = 1; fAddEventMode.SelectedIndex = 0;

            macroData_selectedIndex = -1;
            bMacroActivationKeySet_UpdateLocation();
            macroData_mUpdate();
        }

        private void bMacroActivationKeySet_UpdateLocation()
        {
            Point point = fMacroActivationKey.Location;
            point.X += fMacroActivationKey.Size.Width;
            point.X += fMacroActivationKey.Margin.Right;
            point.Y -= 1;
            bMacroActivationKeySet.Location = point;
        }

        private void dlgt_func1(string param)
        {
            fMacroActivationKey.Text = param;
            bMacroActivationKeySet_UpdateLocation();
            if(Program.mainMacroCollection[Program.selectedMacroIndex].actKey.bVk == WFApp.Key.Mouse.WheelDown ||
               Program.mainMacroCollection[Program.selectedMacroIndex].actKey.bVk == WFApp.Key.Mouse.WheelUp)
                fActivationMethod.SelectedIndex = 1; // switch to Macro.Method.Switch
            fmKeySelect.Close();
        }

        private void dlgt_func2(byte type, string str, int value, int index, bool fc)
        {
            // index == -1 : Add
            // index >=  0 : Insert
            // fc == true : Close Form

            if (fc == true) { fmKeySelect.Close(); return; }

            Macro.Event ev = new Macro.Event();
            if (str != null && value == 0)
            {
                ev.Type = type;
                ev.key.str = str;
                if (index <= -1) macroData_AddItem(ev);
                else if (index >= 0) macroData_AddItem(ev, index);
            }
            else if (value != 0 && str == null)
            {
                ev.Type = type;
                ev.DelayValue = (Int16)value;
                if (index <= -1) macroData_AddItem(ev);
                else if (index >= 0) macroData_AddItem(ev, index);
            }
            else
            {
                MessageBox.Show("MainWindowCtr.dlgt_func2() Error");
                return;
            }

            //macroData_mUpdate();
            //fmKeySelect.Close();
        }

        //------------------------------------------------------------------------------
        //       ***** Events *****
        //------------------------------------------------------------------------------

        private void MainWindowCtr_FormClosing(object sender, FormClosingEventArgs e)
        {
            KeyHandler.Unsubscribe();
            Program.Save();
        }

        private void bNewMacro_Click(object sender, EventArgs e)
        {
            macroData_selectedIndex = -1;
            Macro new_macro = new Macro("New Macro");
            Program.mainMacroCollection.Add(new_macro);
            macroCollection_Update();
            macroCollection.SelectedIndex = macroCollection_indexList.Count - 1;
            group_macroConfig_UpdateState();
        }

        private void bDeleteMacro_Click(object sender, EventArgs e)
        {
            if (Program.selectedMacroIndex == -1) return; // no selection
            if (macroEnabled_indexList.Count > 0)
            {
                // index problem isolated
                MessageBox.Show("Нельзя производить удаление,\nпока есть активные макросы!");
                return;
            }
            Program.mainMacroCollection.RemoveAt(Program.selectedMacroIndex);
            Program.selectedMacroIndex = -1; // ?
            macroCollection_Update();
            group_macroConfig_UpdateState();
        }

        private void bStartMacroRecord_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) return;
            if (e.KeyCode == Keys.Tab) return;
            if (e.KeyCode == Keys.Up) return;
            if (e.KeyCode == Keys.Down) return;
            if (e.KeyCode == Keys.Left) return;
            if (e.KeyCode == Keys.Right) return;
            
            for (int i = 0; i < keyDownBlockList.Count; i++)
                if (keyDownBlockList[i] == e.KeyValue) return;
            keyDownBlockList.Add(e.KeyValue);

            Macro.Event ev_d = new Macro.Event();
            Macro.Event ev_k = new Macro.Event();
            if (eventTimer.IsRunning == false) eventTimer.Start();
            else
            {
                eventTimer.Stop();
                ev_d.Type = Macro.Event.Delay;
                ev_d.DelayValue = (Int16)eventTimer.ElapsedMilliseconds;
                Program.mainMacroCollection[Program.selectedMacroIndex].data.Add(ev_d);
                macroData_AddItem(ev_d);
                eventTimer.Restart();
            }

            ev_k.Type = Macro.Event.KeyDown;
            ev_k.key.Set((byte)e.KeyValue, e.KeyCode.ToString());
            Program.mainMacroCollection[Program.selectedMacroIndex].data.Add(ev_k);
            macroData_AddItem(ev_k);
        }

        private void bStartMacroRecord_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) return;
            if (e.KeyCode == Keys.Tab) return;
            if (e.KeyCode == Keys.Up) return;
            if (e.KeyCode == Keys.Down) return;
            if (e.KeyCode == Keys.Left) return;
            if (e.KeyCode == Keys.Right) return;

            if (keyDownBlockList.Count == 0)
            {
                MessageBox.Show("bStartMacroRecord_KeyUp() Error #1");
                return;
            }
            keyDownBlockList.Remove(e.KeyValue);

            Macro.Event ev_d = new Macro.Event();
            Macro.Event ev_k = new Macro.Event();
            if (eventTimer.IsRunning == false)
            {
                MessageBox.Show("bStartMacroRecord_KeyUp() Error #2");
                return;
            }
            if (eventTimer.IsRunning == true)
            {
                eventTimer.Stop();
                ev_d.Type = Macro.Event.Delay;
                ev_d.DelayValue = (Int16)eventTimer.ElapsedMilliseconds;
                Program.mainMacroCollection[Program.selectedMacroIndex].data.Add(ev_d);
                macroData_AddItem(ev_d);
                eventTimer.Restart();
            }

            ev_k.Type = Macro.Event.KeyUp;
            ev_k.key.Set((byte)e.KeyValue, e.KeyCode.ToString());
            Program.mainMacroCollection[Program.selectedMacroIndex].data.Add(ev_k);
            macroData_AddItem(ev_k);
        }

        private void bStartMacroRecord_Leave(object sender, EventArgs e)
        {
            if (eventTimer.IsRunning == true) eventTimer.Reset();
        }

        private void bStopMacroRecord_Click(object sender, EventArgs e)
        {
            macroData.Update();
        }

        private void fUseVirtualKeyCodes_CheckedChanged(object sender, EventArgs e)
        {
            if (fUseVirtualKeyCodes.Checked == true) KeyHandler.useVirtualKeyCodes = true;
            else KeyHandler.useVirtualKeyCodes = false;
        }

        private void f_mwTimeout_ValueChanged(object sender, EventArgs e)
        {
            KeyHandler.mw_timeout = (int)f_mwTimeout.Value;
        }

        private void fDisableMainMouseBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (fDisableMainMouseBtn.Checked == true) KeyHandler.ignoreMouseButtonsLR = true;
            else KeyHandler.ignoreMouseButtonsLR = false;
        }

        // group_macroCollection -------------------------------------------------------

        private void macroCollection_SelectedValueChanged(object sender, EventArgs e)
        {
            if (macroCollection.SelectedIndex == -1) return;
            Program.selectedMacroIndex = macroCollection_indexList[macroCollection.SelectedIndex];
            group_macroConfig_UpdateState();
            group_macroConfig_FillData();
            macroCollection_Update();
        }

        private void bMacroEnable_Click(object sender, EventArgs e)
        {
            if (Program.selectedMacroIndex == -1) return; // no selection
            Program.mainMacroCollection[Program.selectedMacroIndex].enabled = true;
            KeyHandler.enableSignals.Add(Program.selectedMacroIndex); // send signal
            Program.selectedMacroIndex = -1; // ?
            group_macroConfig_UpdateState();
            macroCollection_Update();
            macroEnabled_Update();
        }

        private void bMacroDisable_Click(object sender, EventArgs e)
        {
            if (macroEnabled.SelectedIndex == -1) return; // no selection
            int index = macroEnabled_indexList[macroEnabled.SelectedIndex];
            Program.mainMacroCollection[index].enabled = false;
            KeyHandler.disableSignals.Add(index); // send signal
            //group_macroConfig_UpdateState();
            macroCollection_Update();
            macroEnabled_Update();
        }

        // group_macroConfig -----------------------------------------------------------

        private void fMacroName_TextChanged(object sender, EventArgs e)
        {
            if (Program.selectedMacroIndex == -1) return; // no selection
            Program.mainMacroCollection[Program.selectedMacroIndex].Name = fMacroName.Text;
            macroCollection_Update();
        }

        private void bMacroActivationKeySet_KeyDownEvent(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (Program.selectedMacroIndex == -1) return;
            if (fExtendedButtonSelection.Checked == false)
            {
                Program.mainMacroCollection[Program.selectedMacroIndex].actKey.Set((byte)e.KeyValue, e.KeyCode.ToString());
                fMacroActivationKey.Text = e.KeyCode.ToString();
                bMacroActivationKeySet_UpdateLocation();
            }
            ResetFocus();
        }

        private void bMacroActivationKeySet_Click(object sender, EventArgs e)
        {
            if (Program.selectedMacroIndex == -1) return;
            if (fExtendedButtonSelection.Checked == true)
            {
                ResetFocus();
                fmKeySelect = new wxMacro.KeySelectDialogEx(dlgt_func1, null, 0);
                fmKeySelect.Show();
            }
        }

        private void fActivationMethod_SelectedValueChanged(object sender, EventArgs e)
        {
            ResetFocus();
            if (Program.selectedMacroIndex == -1) return; // no selection
            Program.mainMacroCollection[Program.selectedMacroIndex].ActMethod = fActivationMethod.SelectedIndex;
            if ((Program.mainMacroCollection[Program.selectedMacroIndex].actKey.bVk == WFApp.Key.Mouse.WheelDown ||
                Program.mainMacroCollection[Program.selectedMacroIndex].actKey.bVk == WFApp.Key.Mouse.WheelUp) &&
                fActivationMethod.SelectedIndex == Macro.Method.Hold)
            {
                MessageBox.Show("События колеса мышки всегда обрабатываются\nв режиме переключение, игнорируя настройки.");
            }
        }

        private void fMacroRepeat_CheckedChanged(object sender, EventArgs e)
        {
            if (Program.selectedMacroIndex == -1) return; // no selection
            Program.mainMacroCollection[Program.selectedMacroIndex].repeat = fMacroRepeat.Checked;
        }

        private void macroData_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Program.selectedMacroIndex == -1) return;
            if (macroData.SelectedIndex == -1) return;
            macroData_selectedIndex = macroData.SelectedIndex;

            if (Program.mainMacroCollection[Program.selectedMacroIndex].data[macroData_selectedIndex].Type == Macro.Event.Delay)
            {
                fEventDelay.Enabled = true;
                fEventDelay.Value = (decimal)Program.mainMacroCollection[Program.selectedMacroIndex].data[macroData_selectedIndex].DelayValue;
            }
            else if (fEventDelay.Enabled == true) fEventDelay.Enabled = false;

            //macroData_mUpdate();
            ResetFocus();
        }

        private void fEventDelay_ValueChanged(object sender, EventArgs e)
        {
            if (Program.selectedMacroIndex == -1) return;
            if (macroData_selectedIndex == -1) return;
            if (Program.mainMacroCollection[Program.selectedMacroIndex].data[macroData_selectedIndex].Type != Macro.Event.Delay) return;
            Program.mainMacroCollection[Program.selectedMacroIndex].data[macroData_selectedIndex].DelayValue = (Int16)fEventDelay.Value;

            macroData.Items.RemoveAt(macroData_selectedIndex);
            macroData_AddItem(Program.mainMacroCollection[Program.selectedMacroIndex].data[macroData_selectedIndex], macroData_selectedIndex);

            //macroData_mUpdate();
            //macroData.Update();
        }

        private void fPreventTermination_CheckedChanged(object sender, EventArgs e)
        {
            if (fPreventTermination.Checked == true) Program.mainMacroCollection[Program.selectedMacroIndex].preventTermination = true;
            else Program.mainMacroCollection[Program.selectedMacroIndex].preventTermination = false;
        }

        private void fEventDelayMultiplier_ValueChanged(object sender, EventArgs e)
        {
            if (Program.selectedMacroIndex == -1) return;
            Program.mainMacroCollection[Program.selectedMacroIndex].delayMultiplier = (float)fEventDelayMultiplier.Value;
            macroData_mUpdate();
        }

        private void bMacroDataDelete_Click(object sender, EventArgs e)
        {
            if (macroData_selectedIndex == -1) return;
            macroData.Items.RemoveAt(macroData_selectedIndex);
            Program.mainMacroCollection[Program.selectedMacroIndex].data.RemoveAt(macroData_selectedIndex);
            if (macroData_selectedIndex >= Program.mainMacroCollection[Program.selectedMacroIndex].data.Count)
                macroData_selectedIndex = Program.mainMacroCollection[Program.selectedMacroIndex].data.Count - 1;
            if (Program.mainMacroCollection[Program.selectedMacroIndex].data.Count == 0) macroData_selectedIndex = -1;
        }

        private void bMacroDataClear_Click(object sender, EventArgs e)
        {
            Program.mainMacroCollection[Program.selectedMacroIndex].data.Clear();
            macroData_mUpdate();
        }

        private void fAddEventSel_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetFocus();
        }

        private void bAddEvent_Click(object sender, EventArgs e)
        {
            if (Program.selectedMacroIndex == -1) return;
            if (fAddEventSel.SelectedIndex == -1) return;

            if (fAddEventSel.SelectedIndex == 1)
            {
                ResetFocus();
                fmKeySelect = new wxMacro.KeySelectDialogEx(null, dlgt_func2, 1);
                fmKeySelect.Show();
            }
            else if (fAddEventSel.SelectedIndex == 0)
            {
                WFApp.Macro.Event ev = new WFApp.Macro.Event();
                ev.Type = WFApp.Macro.Event.Delay;
                ev.DelayValue = 100;
                if (macroData_selectedIndex == -1)
                {
                    Program.mainMacroCollection[Program.selectedMacroIndex].data.Add(ev);
                    macroData_AddItem(ev);
                }
                else // macroData_selectedIndex != -1
                {
                    int ind = macroData_selectedIndex;
                    if (insertMode == 0) // after
                    {
                        ind += 1;
                        if (WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Count == ind + 1) // add
                        {
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev);
                            macroData_AddItem(ev);
                        }
                        else // insert
                        {
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind, ev);
                            macroData_AddItem(ev, ind);
                        }
                    }
                    else // before - insert
                    {
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind, ev);
                        macroData_AddItem(ev, ind);
                    }
                }
            }
        }

        private void fAddEventMode_SelectedValueChanged(object sender, EventArgs e)
        {
            if (fAddEventMode.SelectedIndex == -1) insertMode = 0;
            if (fAddEventMode.SelectedIndex == 0) insertMode = 0;
            if (fAddEventMode.SelectedIndex == 1) insertMode = 1;
            ResetFocus();
        }

        private void fAddEventMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetFocus();
        }

        // Debug -----------------------------------------------------------------------

        private void debug_mainMacroCollection_Click(object sender, EventArgs e)
        {
            string str = "selectedMacroIndex:" + Program.selectedMacroIndex + "\n";
            str += "mainMacroCollection.Count: " + Program.mainMacroCollection.Count + "\n";
            for (int i = 0; i < Program.mainMacroCollection.Count; i++)
            {
                str += "["+i+"] e:" + Program.mainMacroCollection[i].enabled;
                str += " a:" + Program.mainMacroCollection[i].active;
                str += " / am:" + Program.mainMacroCollection[i].ActMethod;
                str += " ak:" + Program.mainMacroCollection[i].actKey.bVk;
                str += "(" + Program.mainMacroCollection[i].actKey.bScan + ")";
                str += " / r:" + Program.mainMacroCollection[i].repeat;
                str += " pt:" + Program.mainMacroCollection[i].preventTermination;
                str += " dm:" + Program.mainMacroCollection[i].delayMultiplier;
                str += " / " + Program.mainMacroCollection[i].Name + "\n";
            }
            MessageBox.Show(str);
        }

        private void debug_macroCollection_IndexList_Click(object sender, EventArgs e)
        {
            string str = "macroCollection_IndexList.Count: " + macroCollection_indexList.Count + "\n";
            for (int i = 0; i < macroCollection_indexList.Count; i++)
                str += "index:" + i + " / data_pointer:" + macroCollection_indexList[i] + "\n";
            MessageBox.Show(str);
        }

        private void debug_macroEnabled_indexList_Click(object sender, EventArgs e)
        {
            string str = "macroEnabled_indexList.Count: " + macroEnabled_indexList.Count + "\n";
            for (int i = 0; i < macroEnabled_indexList.Count; i++)
                str += "index:" + i + " / data_pointer:" + macroEnabled_indexList[i] + "\n";
            MessageBox.Show(str);
        }

        private void debug_macroData_Click(object sender, EventArgs e)
        {
            string str = "selectedMacroIndex:" + Program.selectedMacroIndex + "\n";
            if (Program.selectedMacroIndex != -1)
            {
                str += "macroData selected index:" + macroData_selectedIndex + "\n";
                str += "data.Count:" + Program.mainMacroCollection[Program.selectedMacroIndex].data.Count + "\n";
                if (Program.selectedMacroIndex != -1) // no selection
                    for (int i = 0; i < Program.mainMacroCollection[Program.selectedMacroIndex].data.Count; i++)
                    {
                        str += "Event[" + i + "]: " + Program.mainMacroCollection[Program.selectedMacroIndex].data[i].key.str;
                        str += " (" + Program.mainMacroCollection[Program.selectedMacroIndex].data[i].key.bScan + ")";
                        str += " / Type: " + Program.mainMacroCollection[Program.selectedMacroIndex].data[i].Type;
                        str += " / DelayValue: " + Program.mainMacroCollection[Program.selectedMacroIndex].data[i].DelayValue + "\n";
                    }
            }
            MessageBox.Show(str);
        }

        private void debug_enabledIndexListAndSignals_Click(object sender, EventArgs e)
        {
            string str = "";
            str += "enableSignals.Count: " + KeyHandler.enableSignals.Count + "\n";
            for (int i = 0; i < KeyHandler.enableSignals.Count; i++)
                str += "enableSignals[" + i + "]: " + KeyHandler.enableSignals[i] + "\n";

            str += "disableSignals.Count: " + KeyHandler.disableSignals.Count + "\n";
            for (int i = 0; i < KeyHandler.disableSignals.Count; i++)
                str += "disableSignals[" + i + "]: " + KeyHandler.disableSignals[i] + "\n";

            str += "\nenabledIndexList.Count: " + KeyHandler.enabledIndexList.Count + "\n";
            for (int i = 0; i < KeyHandler.enabledIndexList.Count; i++)
                str += "enabledIndexList[" + i + "]: " + KeyHandler.enabledIndexList[i] + "\n";

            MessageBox.Show(str);
        }

        private void debug_TFexecTime_Click(object sender, EventArgs e)
        {
            string str = "Handler loop iteration time:\n\n";
            str += "TF: " + KeyHandler.tf_timer.Ticks + " ticks / " + KeyHandler.tf_timer.ms + " ms\n";
            str += "HK: " + KeyHandler.hk_timer.Ticks + " ticks / " + KeyHandler.hk_timer.ms + " ms\n";
            str += "HM: " + KeyHandler.hm_timer.Ticks + " ticks / " + KeyHandler.hm_timer.ms + " ms\n";
            MessageBox.Show(str);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.Save();
        }
    }
}
