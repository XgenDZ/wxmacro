﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wxMacro
{
    public partial class KeySelectDialogEx : Form
    {
        private int mode;
        private WFApp.DelegateActKey dlgt1;
        private WFApp.DelegateMacroData dlgt2;

        public KeySelectDialogEx(WFApp.DelegateActKey sender1, WFApp.DelegateMacroData sender2, int mode)
        {
            dlgt1 = sender1;
            dlgt2 = sender2;
            this.mode = mode;
            InitializeComponent();

            if (mode == 1)
            {
                lDurationT.Enabled = true;
                fDuration.Enabled = true;
            }
        }

        private void fKey_SelectedIndexChanged(object sender, EventArgs e)
        {
            lKeyT.Focus();
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            if (mode == 0) // act key
            {
                if (fKey.SelectedIndex == -1) return;
                if (fKey.SelectedIndex == 0)
                {
                    WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].actKey.Set(WFApp.Key.Mouse.Left, "L Mouse");
                    dlgt1("L Mouse");
                }
                else if (fKey.SelectedIndex == 1)
                {
                    WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].actKey.Set(WFApp.Key.Mouse.Right, "R Mouse");
                    dlgt1("R Mouse");
                }
                else if (fKey.SelectedIndex == 2)
                {
                    WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].actKey.Set(WFApp.Key.Mouse.WheelDown, "MW Down");
                    dlgt1("MW Down");
                }
                else if (fKey.SelectedIndex == 3)
                {
                    WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].actKey.Set(WFApp.Key.Mouse.WheelUp, "MW Up");
                    dlgt1("MW Up");
                }
                else if (fKey.SelectedIndex == 4)
                {
                    WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].actKey.Set(WFApp.Key.Mouse.XButton1, "X1 Mouse");
                    dlgt1("X1 Mouse");
                }
                else if (fKey.SelectedIndex == 5)
                {
                    WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].actKey.Set(WFApp.Key.Mouse.XButton2, "X2 Mouse");
                    dlgt1("X2 Mouse");
                }
                else if (fKey.SelectedIndex == 6)
                {
                    WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].actKey.Set(0x25, "Left Ar");
                    dlgt1("Left Ar");
                }
                else if (fKey.SelectedIndex == 7)
                {
                    WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].actKey.Set(0x27, "Right Ar");
                    dlgt1("Right Ar");
                }
                else if (fKey.SelectedIndex == 8)
                {
                    WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].actKey.Set(0x28, "Down Ar");
                    dlgt1("Down Ar");
                }
                else if (fKey.SelectedIndex == 9)
                {
                    WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].actKey.Set(0x26, "Up Ar");
                    dlgt1("Up Ar");
                }
                else if (fKey.SelectedIndex == 10)
                {
                    WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].actKey.Set(0x0D, "Enter");
                    dlgt1("Enter");
                }
                else if (fKey.SelectedIndex == 11)
                {
                    WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].actKey.Set(0x09, "Tab");
                    dlgt1("Tab");
                }
            }
            else if (mode == 1) // macro data
            {
                if (fKey.SelectedIndex == -1) return;
                if (fKey.SelectedIndex == 0) // LMB
                {
                    int key_vk = WFApp.Key.Mouse.Left;
                    string str = "Left Mouse Button";

                    WFApp.Macro.Event ev_1 = new WFApp.Macro.Event();
                    WFApp.Macro.Event ev_d = new WFApp.Macro.Event();
                    WFApp.Macro.Event ev_2 = new WFApp.Macro.Event();
                    ev_1.Type = WFApp.Macro.Event.KeyDown;
                    ev_d.Type = WFApp.Macro.Event.Delay;
                    ev_2.Type = WFApp.Macro.Event.KeyUp;
                    ev_d.DelayValue = (Int16)fDuration.Value;
                    ev_1.key = new WFApp.Key(key_vk, str);
                    ev_2.key = new WFApp.Key(key_vk, str);
                    if (WFApp.MainWindowCtr.macroData_selectedIndex == -1)
                    {
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_1);
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_d);
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_2);
                        dlgt2(ev_1.Type, ev_1.key.str, 0, -1, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, -1, false);
                        dlgt2(ev_2.Type, ev_2.key.str, 0, -1, false);
                    }
                    else // macroData_selectedIndex != -1
                    {
                        int ind = WFApp.MainWindowCtr.macroData_selectedIndex;
                        if (WFApp.MainWindowCtr.insertMode == 0) // after
                        {
                            ind += 1;
                            if (WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Count == ind - 1) // add
                            {
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_1);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_d);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_2);
                                dlgt2(ev_1.Type, ev_1.key.str, 0, -1, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, -1, false);
                                dlgt2(ev_2.Type, ev_2.key.str, 0, -1, false);
                            }
                            else // insert
                            {
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind, ev_1);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 1, ev_d);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 2, ev_2);
                                dlgt2(ev_1.Type, ev_1.key.str, 0, ind, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, ind + 1, false);
                                dlgt2(ev_2.Type, ev_2.key.str, 0, ind + 2, false);
                            }
                        }
                        else // before - insert
                        {
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind, ev_1);
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 1, ev_d);
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 2, ev_2);
                            dlgt2(ev_1.Type, ev_1.key.str, 0, ind, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, ind + 1, false);
                            dlgt2(ev_2.Type, ev_2.key.str, 0, ind + 2, false);
                        }
                    }

                    dlgt2(0, null, 0, 0, true); // close form
                }
                else if (fKey.SelectedIndex == 1) // RMB
                {
                    int key_vk = WFApp.Key.Mouse.Right;
                    string str = "Right Mouse Button";
                    WFApp.Macro.Event ev_1 = new WFApp.Macro.Event();
                    WFApp.Macro.Event ev_d = new WFApp.Macro.Event();
                    WFApp.Macro.Event ev_2 = new WFApp.Macro.Event();
                    ev_1.Type = WFApp.Macro.Event.KeyDown;
                    ev_d.Type = WFApp.Macro.Event.Delay;
                    ev_2.Type = WFApp.Macro.Event.KeyUp;
                    ev_d.DelayValue = (Int16)fDuration.Value;
                    ev_1.key = new WFApp.Key(key_vk, str);
                    ev_2.key = new WFApp.Key(key_vk, str);
                    if (WFApp.MainWindowCtr.macroData_selectedIndex == -1)
                    {
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_1);
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_d);
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_2);
                        dlgt2(ev_1.Type, ev_1.key.str, 0, -1, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, -1, false);
                        dlgt2(ev_2.Type, ev_2.key.str, 0, -1, false);
                    }
                    else // macroData_selectedIndex != -1
                    {
                        int ind = WFApp.MainWindowCtr.macroData_selectedIndex;
                        if (WFApp.MainWindowCtr.insertMode == 0) // after
                        {
                            ind += 1;
                            if (WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Count == ind - 1) // add
                            {
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_1);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_d);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_2);
                                dlgt2(ev_1.Type, ev_1.key.str, 0, -1, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, -1, false);
                                dlgt2(ev_2.Type, ev_2.key.str, 0, -1, false);
                            }
                            else // insert
                            {
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind, ev_1);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 1, ev_d);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 2, ev_2);
                                dlgt2(ev_1.Type, ev_1.key.str, 0, ind, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, ind + 1, false);
                                dlgt2(ev_2.Type, ev_2.key.str, 0, ind + 2, false);
                            }
                        }
                        else // before - insert
                        {
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind, ev_1);
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 1, ev_d);
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 2, ev_2);
                            dlgt2(ev_1.Type, ev_1.key.str, 0, ind, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, ind + 1, false);
                            dlgt2(ev_2.Type, ev_2.key.str, 0, ind + 2, false);
                        }
                    }

                    dlgt2(0, null, 0, 0, true); // close form
                }
                else if (fKey.SelectedIndex == 2)
                {
                    //
                }
                else if (fKey.SelectedIndex == 3)
                {
                    //
                }
                else if (fKey.SelectedIndex == 4)
                {
                    //
                }
                else if (fKey.SelectedIndex == 5)
                {
                    //
                }
                else if (fKey.SelectedIndex == 6) // Left arrow (0x25)
                {
                    int key_vk = 0x25;
                    string str = "Left Arrow";
                    WFApp.Macro.Event ev_1 = new WFApp.Macro.Event();
                    WFApp.Macro.Event ev_d = new WFApp.Macro.Event();
                    WFApp.Macro.Event ev_2 = new WFApp.Macro.Event();
                    ev_1.Type = WFApp.Macro.Event.KeyDown;
                    ev_d.Type = WFApp.Macro.Event.Delay;
                    ev_2.Type = WFApp.Macro.Event.KeyUp;
                    ev_d.DelayValue = (Int16)fDuration.Value;
                    ev_1.key = new WFApp.Key(key_vk, str);
                    ev_2.key = new WFApp.Key(key_vk, str);
                    if (WFApp.MainWindowCtr.macroData_selectedIndex == -1)
                    {
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_1);
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_d);
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_2);
                        dlgt2(ev_1.Type, ev_1.key.str, 0, -1, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, -1, false);
                        dlgt2(ev_2.Type, ev_2.key.str, 0, -1, false);
                    }
                    else // macroData_selectedIndex != -1
                    {
                        int ind = WFApp.MainWindowCtr.macroData_selectedIndex;
                        if (WFApp.MainWindowCtr.insertMode == 0) // after
                        {
                            ind += 1;
                            if (WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Count == ind - 1) // add
                            {
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_1);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_d);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_2);
                                dlgt2(ev_1.Type, ev_1.key.str, 0, -1, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, -1, false);
                                dlgt2(ev_2.Type, ev_2.key.str, 0, -1, false);
                            }
                            else // insert
                            {
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind, ev_1);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 1, ev_d);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 2, ev_2);
                                dlgt2(ev_1.Type, ev_1.key.str, 0, ind, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, ind + 1, false);
                                dlgt2(ev_2.Type, ev_2.key.str, 0, ind + 2, false);
                            }
                        }
                        else // before - insert
                        {
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind, ev_1);
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 1, ev_d);
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 2, ev_2);
                            dlgt2(ev_1.Type, ev_1.key.str, 0, ind, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, ind + 1, false);
                            dlgt2(ev_2.Type, ev_2.key.str, 0, ind + 2, false);
                        }
                    }

                    dlgt2(0, null, 0, 0, true); // close form
                }
                else if (fKey.SelectedIndex == 7) // Right arrow (0x27)
                {
                    int key_vk = 0x27;
                    string str = "Right Arrow";
                    WFApp.Macro.Event ev_1 = new WFApp.Macro.Event();
                    WFApp.Macro.Event ev_d = new WFApp.Macro.Event();
                    WFApp.Macro.Event ev_2 = new WFApp.Macro.Event();
                    ev_1.Type = WFApp.Macro.Event.KeyDown;
                    ev_d.Type = WFApp.Macro.Event.Delay;
                    ev_2.Type = WFApp.Macro.Event.KeyUp;
                    ev_d.DelayValue = (Int16)fDuration.Value;
                    ev_1.key = new WFApp.Key(key_vk, str);
                    ev_2.key = new WFApp.Key(key_vk, str);
                    if (WFApp.MainWindowCtr.macroData_selectedIndex == -1)
                    {
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_1);
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_d);
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_2);
                        dlgt2(ev_1.Type, ev_1.key.str, 0, -1, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, -1, false);
                        dlgt2(ev_2.Type, ev_2.key.str, 0, -1, false);
                    }
                    else // macroData_selectedIndex != -1
                    {
                        int ind = WFApp.MainWindowCtr.macroData_selectedIndex;
                        if (WFApp.MainWindowCtr.insertMode == 0) // after
                        {
                            ind += 1;
                            if (WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Count == ind - 1) // add
                            {
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_1);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_d);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_2);
                                dlgt2(ev_1.Type, ev_1.key.str, 0, -1, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, -1, false);
                                dlgt2(ev_2.Type, ev_2.key.str, 0, -1, false);
                            }
                            else // insert
                            {
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind, ev_1);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 1, ev_d);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 2, ev_2);
                                dlgt2(ev_1.Type, ev_1.key.str, 0, ind, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, ind + 1, false);
                                dlgt2(ev_2.Type, ev_2.key.str, 0, ind + 2, false);
                            }
                        }
                        else // before - insert
                        {
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind, ev_1);
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 1, ev_d);
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 2, ev_2);
                            dlgt2(ev_1.Type, ev_1.key.str, 0, ind, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, ind + 1, false);
                            dlgt2(ev_2.Type, ev_2.key.str, 0, ind + 2, false);
                        }
                    }

                    dlgt2(0, null, 0, 0, true); // close form
                }
                else if (fKey.SelectedIndex == 8) // Down arrow (0x28)
                {
                    int key_vk = 0x28;
                    string str = "Down Arrow";
                    WFApp.Macro.Event ev_1 = new WFApp.Macro.Event();
                    WFApp.Macro.Event ev_d = new WFApp.Macro.Event();
                    WFApp.Macro.Event ev_2 = new WFApp.Macro.Event();
                    ev_1.Type = WFApp.Macro.Event.KeyDown;
                    ev_d.Type = WFApp.Macro.Event.Delay;
                    ev_2.Type = WFApp.Macro.Event.KeyUp;
                    ev_d.DelayValue = (Int16)fDuration.Value;
                    ev_1.key = new WFApp.Key(key_vk, str);
                    ev_2.key = new WFApp.Key(key_vk, str);
                    if (WFApp.MainWindowCtr.macroData_selectedIndex == -1)
                    {
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_1);
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_d);
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_2);
                        dlgt2(ev_1.Type, ev_1.key.str, 0, -1, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, -1, false);
                        dlgt2(ev_2.Type, ev_2.key.str, 0, -1, false);
                    }
                    else // macroData_selectedIndex != -1
                    {
                        int ind = WFApp.MainWindowCtr.macroData_selectedIndex;
                        if (WFApp.MainWindowCtr.insertMode == 0) // after
                        {
                            ind += 1;
                            if (WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Count == ind - 1) // add
                            {
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_1);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_d);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_2);
                                dlgt2(ev_1.Type, ev_1.key.str, 0, -1, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, -1, false);
                                dlgt2(ev_2.Type, ev_2.key.str, 0, -1, false);
                            }
                            else // insert
                            {
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind, ev_1);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 1, ev_d);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 2, ev_2);
                                dlgt2(ev_1.Type, ev_1.key.str, 0, ind, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, ind + 1, false);
                                dlgt2(ev_2.Type, ev_2.key.str, 0, ind + 2, false);
                            }
                        }
                        else // before - insert
                        {
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind, ev_1);
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 1, ev_d);
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 2, ev_2);
                            dlgt2(ev_1.Type, ev_1.key.str, 0, ind, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, ind + 1, false);
                            dlgt2(ev_2.Type, ev_2.key.str, 0, ind + 2, false);
                        }
                    }

                    dlgt2(0, null, 0, 0, true); // close form
                }
                else if (fKey.SelectedIndex == 9) // Up arrow (0x26)
                {
                    int key_vk = 0x26;
                    string str = "Up Arrow";
                    WFApp.Macro.Event ev_1 = new WFApp.Macro.Event();
                    WFApp.Macro.Event ev_d = new WFApp.Macro.Event();
                    WFApp.Macro.Event ev_2 = new WFApp.Macro.Event();
                    ev_1.Type = WFApp.Macro.Event.KeyDown;
                    ev_d.Type = WFApp.Macro.Event.Delay;
                    ev_2.Type = WFApp.Macro.Event.KeyUp;
                    ev_d.DelayValue = (Int16)fDuration.Value;
                    ev_1.key = new WFApp.Key(key_vk, str);
                    ev_2.key = new WFApp.Key(key_vk, str);
                    if (WFApp.MainWindowCtr.macroData_selectedIndex == -1)
                    {
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_1);
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_d);
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_2);
                        dlgt2(ev_1.Type, ev_1.key.str, 0, -1, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, -1, false);
                        dlgt2(ev_2.Type, ev_2.key.str, 0, -1, false);
                    }
                    else // macroData_selectedIndex != -1
                    {
                        int ind = WFApp.MainWindowCtr.macroData_selectedIndex;
                        if (WFApp.MainWindowCtr.insertMode == 0) // after
                        {
                            ind += 1;
                            if (WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Count == ind - 1) // add
                            {
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_1);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_d);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_2);
                                dlgt2(ev_1.Type, ev_1.key.str, 0, -1, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, -1, false);
                                dlgt2(ev_2.Type, ev_2.key.str, 0, -1, false);
                            }
                            else // insert
                            {
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind, ev_1);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 1, ev_d);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 2, ev_2);
                                dlgt2(ev_1.Type, ev_1.key.str, 0, ind, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, ind + 1, false);
                                dlgt2(ev_2.Type, ev_2.key.str, 0, ind + 2, false);
                            }
                        }
                        else // before - insert
                        {
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind, ev_1);
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 1, ev_d);
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 2, ev_2);
                            dlgt2(ev_1.Type, ev_1.key.str, 0, ind, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, ind + 1, false);
                            dlgt2(ev_2.Type, ev_2.key.str, 0, ind + 2, false);
                        }
                    }

                    dlgt2(0, null, 0, 0, true); // close form
                }
                else if (fKey.SelectedIndex == 10)  // Enter (0x0D)
                {
                    int key_vk = 0x0D;
                    string str = "Enter";
                    WFApp.Macro.Event ev_1 = new WFApp.Macro.Event();
                    WFApp.Macro.Event ev_d = new WFApp.Macro.Event();
                    WFApp.Macro.Event ev_2 = new WFApp.Macro.Event();
                    ev_1.Type = WFApp.Macro.Event.KeyDown;
                    ev_d.Type = WFApp.Macro.Event.Delay;
                    ev_2.Type = WFApp.Macro.Event.KeyUp;
                    ev_d.DelayValue = (Int16)fDuration.Value;
                    ev_1.key = new WFApp.Key(key_vk, str);
                    ev_2.key = new WFApp.Key(key_vk, str);
                    if (WFApp.MainWindowCtr.macroData_selectedIndex == -1)
                    {
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_1);
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_d);
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_2);
                        dlgt2(ev_1.Type, ev_1.key.str, 0, -1, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, -1, false);
                        dlgt2(ev_2.Type, ev_2.key.str, 0, -1, false);
                    }
                    else // macroData_selectedIndex != -1
                    {
                        int ind = WFApp.MainWindowCtr.macroData_selectedIndex;
                        if (WFApp.MainWindowCtr.insertMode == 0) // after
                        {
                            ind += 1;
                            if (WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Count == ind - 1) // add
                            {
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_1);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_d);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_2);
                                dlgt2(ev_1.Type, ev_1.key.str, 0, -1, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, -1, false);
                                dlgt2(ev_2.Type, ev_2.key.str, 0, -1, false);
                            }
                            else // insert
                            {
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind, ev_1);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 1, ev_d);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 2, ev_2);
                                dlgt2(ev_1.Type, ev_1.key.str, 0, ind, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, ind + 1, false);
                                dlgt2(ev_2.Type, ev_2.key.str, 0, ind + 2, false);
                            }
                        }
                        else // before - insert
                        {
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind, ev_1);
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 1, ev_d);
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 2, ev_2);
                            dlgt2(ev_1.Type, ev_1.key.str, 0, ind, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, ind + 1, false);
                            dlgt2(ev_2.Type, ev_2.key.str, 0, ind + 2, false);
                        }
                    }

                    dlgt2(0, null, 0, 0, true); // close form
                }
                else if (fKey.SelectedIndex == 11) // Tab (0x09)
                {
                    int key_vk = 0x09;
                    string str = "Tab";
                    WFApp.Macro.Event ev_1 = new WFApp.Macro.Event();
                    WFApp.Macro.Event ev_d = new WFApp.Macro.Event();
                    WFApp.Macro.Event ev_2 = new WFApp.Macro.Event();
                    ev_1.Type = WFApp.Macro.Event.KeyDown;
                    ev_d.Type = WFApp.Macro.Event.Delay;
                    ev_2.Type = WFApp.Macro.Event.KeyUp;
                    ev_d.DelayValue = (Int16)fDuration.Value;
                    ev_1.key = new WFApp.Key(key_vk, str);
                    ev_2.key = new WFApp.Key(key_vk, str);
                    if (WFApp.MainWindowCtr.macroData_selectedIndex == -1)
                    {
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_1);
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_d);
                        WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_2);
                        dlgt2(ev_1.Type, ev_1.key.str, 0, -1, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, -1, false);
                        dlgt2(ev_2.Type, ev_2.key.str, 0, -1, false);
                    }
                    else // macroData_selectedIndex != -1
                    {
                        int ind = WFApp.MainWindowCtr.macroData_selectedIndex;
                        if (WFApp.MainWindowCtr.insertMode == 0) // after
                        {
                            ind += 1;
                            if (WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Count == ind - 1) // add
                            {
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_1);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_d);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Add(ev_2);
                                dlgt2(ev_1.Type, ev_1.key.str, 0, -1, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, -1, false);
                                dlgt2(ev_2.Type, ev_2.key.str, 0, -1, false);
                            }
                            else // insert
                            {
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind, ev_1);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 1, ev_d);
                                WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 2, ev_2);
                                dlgt2(ev_1.Type, ev_1.key.str, 0, ind, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, ind + 1, false);
                                dlgt2(ev_2.Type, ev_2.key.str, 0, ind + 2, false);
                            }
                        }
                        else // before - insert
                        {
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind, ev_1);
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 1, ev_d);
                            WFApp.Program.mainMacroCollection[WFApp.Program.selectedMacroIndex].data.Insert(ind + 2, ev_2);
                            dlgt2(ev_1.Type, ev_1.key.str, 0, ind, false); dlgt2(ev_d.Type, null, ev_d.DelayValue, ind + 1, false);
                            dlgt2(ev_2.Type, ev_2.key.str, 0, ind + 2, false);
                        }
                    }

                    dlgt2(0, null, 0, 0, true); // close form
                }
            }
        }
    }
}