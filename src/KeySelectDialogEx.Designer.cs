﻿namespace wxMacro
{
    partial class KeySelectDialogEx
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lKeyT = new System.Windows.Forms.Label();
            this.fKey = new System.Windows.Forms.ComboBox();
            this.bAdd = new System.Windows.Forms.Button();
            this.lDurationT = new System.Windows.Forms.Label();
            this.fDuration = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.fDuration)).BeginInit();
            this.SuspendLayout();
            // 
            // lKeyT
            // 
            this.lKeyT.AutoSize = true;
            this.lKeyT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lKeyT.Location = new System.Drawing.Point(23, 22);
            this.lKeyT.Name = "lKeyT";
            this.lKeyT.Size = new System.Drawing.Size(30, 15);
            this.lKeyT.TabIndex = 0;
            this.lKeyT.Text = "Key:";
            // 
            // fKey
            // 
            this.fKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fKey.FormattingEnabled = true;
            this.fKey.Items.AddRange(new object[] {
            "Left Mouse Button",
            "Right Mouse Button",
            "Mouse Wheel Down",
            "Mouse Wheel Up",
            "Mouse X Button 1",
            "Mouse X Button 2",
            "Left Arrow",
            "Right Arrow",
            "Down Arrow",
            "Up Arrow",
            "Enter",
            "Tab"});
            this.fKey.Location = new System.Drawing.Point(58, 19);
            this.fKey.Name = "fKey";
            this.fKey.Size = new System.Drawing.Size(138, 23);
            this.fKey.TabIndex = 1;
            this.fKey.SelectedIndexChanged += new System.EventHandler(this.fKey_SelectedIndexChanged);
            // 
            // bAdd
            // 
            this.bAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bAdd.Location = new System.Drawing.Point(202, 19);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(75, 23);
            this.bAdd.TabIndex = 2;
            this.bAdd.Text = "Add";
            this.bAdd.UseVisualStyleBackColor = true;
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // lDurationT
            // 
            this.lDurationT.AutoSize = true;
            this.lDurationT.Enabled = false;
            this.lDurationT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lDurationT.Location = new System.Drawing.Point(23, 54);
            this.lDurationT.Name = "lDurationT";
            this.lDurationT.Size = new System.Drawing.Size(152, 15);
            this.lDurationT.TabIndex = 6;
            this.lDurationT.Text = "Длительность нажатия: ";
            // 
            // fDuration
            // 
            this.fDuration.Enabled = false;
            this.fDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fDuration.Location = new System.Drawing.Point(181, 52);
            this.fDuration.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.fDuration.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.fDuration.Name = "fDuration";
            this.fDuration.Size = new System.Drawing.Size(58, 21);
            this.fDuration.TabIndex = 7;
            this.fDuration.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // KeySelectDialogEx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(307, 90);
            this.Controls.Add(this.fDuration);
            this.Controls.Add(this.lDurationT);
            this.Controls.Add(this.bAdd);
            this.Controls.Add(this.fKey);
            this.Controls.Add(this.lKeyT);
            this.Name = "KeySelectDialogEx";
            this.Text = "KeySelectDialogEx";
            ((System.ComponentModel.ISupportInitialize)(this.fDuration)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lKeyT;
        private System.Windows.Forms.ComboBox fKey;
        private System.Windows.Forms.Button bAdd;
        private System.Windows.Forms.Label lDurationT;
        private System.Windows.Forms.NumericUpDown fDuration;
    }
}