﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

using System.IO;
using System.Text;
using System.Media;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;

using Gma.System.MouseKeyHook; // + NuGet: MouseKeyHook

namespace WFApp
{
    static public class SF
    {
        [DllImport("user32.dll")]
        public static extern short GetAsyncKeyState(int vKey);

        [DllImport("user32.dll")]
        public static extern uint keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

        [DllImport("user32.dll")]
        public static extern uint MapVirtualKey(uint uCode, uint uMapType);

        [DllImport("user32.dll")]
        public static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, UIntPtr dwExtraInfo);

        public static class KeyEvent
        {
            public const int KeyDown = 0;
            public const int KeyUp = 0x2;
        }
        public static class MouseEvent
        {
            public const int LeftDown = 0x02;
            public const int LeftUp = 0x04;
            public const int RightDown = 0x08;
            public const int RightUp = 0x10;
            public const int MiddleDown = 0x20;
            public const int MiddleUp = 0x40;
            public const int WheelRot = 0x0800;
        }
    }

    static class IO
    {
        private static readonly byte[] prog_id = { 0xE2, 0x11 };
        private const byte std_cur = 1; // current IO standart
        public const byte std_1 = 1; // wxMacro IO standart #1

        static public class FileNames
        {
            public const string Database = "macro_data.wxmdb";
        }

        static public class Write
        {
            public static void Std(FileStream fs)
            {
                fs.Write(prog_id, 0, sizeof(byte) * prog_id.Length);
                byte[] barr = new byte[1]; barr[0] = std_cur;
                fs.Write(barr, 0, sizeof(byte));
            }
            public static void Int16(FileStream fs, Int16 data)
            {
                fs.Write(BitConverter.GetBytes(data), 0, sizeof(Int16));
            }
            public static void Int32(FileStream fs, Int32 data)
            {
                fs.Write(BitConverter.GetBytes(data), 0, sizeof(Int32));
            }
            public static void Bool(FileStream fs, bool data)
            {
                fs.Write(BitConverter.GetBytes(data), 0, sizeof(bool));
            }
            public static void Float(FileStream fs, float data)
            {
                fs.Write(BitConverter.GetBytes((double)data), 0, sizeof(double));
            }
            public static bool String(FileStream fs, string data)
            {
                byte length;
                if (data == null) length = 0;
                else if (data.Length > 255) return false;
                else length = (byte)data.Length;
                fs.Write(BitConverter.GetBytes(length), 0, sizeof(byte));
                if (data == null) return true;
                char[] str = data.ToCharArray();
                for (int i = 0; i < length; i++)
                {
                    if (str[i] > 255) return false;
                    fs.Write(BitConverter.GetBytes(str[i]), 0, sizeof(byte));
                }
                return true;
            }
            public static void Byte(FileStream fs, byte data)
            {
                byte[] barr = new byte[1]; barr[0] = data;
                fs.Write(barr, 0, sizeof(byte));
            }
        }

        static public class Read
        {
            private static bool terminate_zero = true;
            public static bool TerminateZero
            {
                set { terminate_zero = value; }
            }

            public static byte Std(FileStream fs)
            {
                byte[] barr = new byte[prog_id.Length];
                fs.Read(barr, 0, sizeof(byte) * prog_id.Length);
                for (int i = 0; i < prog_id.Length; i++)
                    if (barr[i] != prog_id[i]) return 0;
                fs.Read(barr, 0, sizeof(byte));
                return barr[0];
            }
            public static Int16 Int16(FileStream fs)
            {
                byte[] barr = new byte[sizeof(Int16)];
                fs.Read(barr, 0, sizeof(Int16));
                return BitConverter.ToInt16(barr, 0);
            }
            public static Int32 Int32(FileStream fs)
            {
                byte[] barr = new byte[sizeof(Int32)];
                fs.Read(barr, 0, sizeof(Int32));
                return BitConverter.ToInt32(barr, 0);
            }
            public static bool Bool(FileStream fs)
            {
                byte[] barr = new byte[sizeof(bool)];
                fs.Read(barr, 0, sizeof(bool));
                return BitConverter.ToBoolean(barr, 0);
            }
            public static float Float(FileStream fs)
            {
                byte[] barr = new byte[sizeof(double)];
                fs.Read(barr, 0, sizeof(double));
                return (float)BitConverter.ToDouble(barr, 0);
            }
            public static string String(FileStream fs)
            {
                byte[] barr = new byte[255];
                fs.Read(barr, 0, sizeof(byte));
                int length = barr[0];
                if (length == 0) return "";
                for (int i = 0; i < length; i++)
                    fs.Read(barr, i, sizeof(byte));
                if (terminate_zero == false) return Encoding.ASCII.GetString(barr);
                else return Encoding.ASCII.GetString(barr).TrimEnd('\0');
            }
            public static byte Byte(FileStream fs)
            {
                byte[] barr = new byte[1];
                fs.Read(barr, 0, sizeof(byte));
                return barr[0];
            }
        }

        static public void ShowTypesSize()
        {
            string str = "Int:" + sizeof(int) + "\n";
            str += "Int32:" + sizeof(Int32) + "\n";
            str += "Float:" + sizeof(float) + "\n";
            str += "Double:" + sizeof(double) + "\n";
            str += "bool:" + sizeof(bool) + "\n";
            str += "Boolean:" + sizeof(Boolean) + "\n";
            str += "char:" + sizeof(char);
            MessageBox.Show(str);
        }

        /* wxMacro IO standart 1
         * 
         * DataBase file pattern:
         *   [..][.][.][ macro-data-1 ][ macro-data-2 ][ macro-data-3 ] ... <EOF>
         *    |   |  |
         *    |   | number of macro-data records (byte)
         *    |   |
         *    |  wxMacro IO standart id (byte)
         *    |
         *   application id signature: 0xE2 0x11
         * 
         * 
         * macro-data segment pattern:
         *   [.][.][.][........][.][ ... ][.][....][.][ ... ][ event-1 ][ event-2 ] ...
         *    |  |  |    |  8    |   | n   |   |4   |   | n
         *    |  |  |    |       |   |     |   |    |   |
         *    |  |  |    |       |   |     |   |    |  activation key name string (array of char) (char = 1 byte)
         *    |  |  |    |       |   |     |   |    |
         *    |  |  |    |       |   |     |   |   activation key string length (byte)
         *    |  |  |    |       |   |     |   |
         *    |  |  |    |       |   |     |  activation key: bVk (Int32)
         *    |  |  |    |       |   |     |
         *    |  |  |    |       |   |     activation method (byte)
         *    |  |  |    |       |   |
         *    |  |  |    |       |  macro name string (array of char) (char = 1 byte)
         *    |  |  |    |       |
         *    |  |  |    |      macro name string length (byte)
         *    |  |  |    |
         *    |  |  |   global delay multiplier (double)
         *    |  |  |
         *    |  | prevent termination value (bool)
         *    |  |
         *    | repeat value (bool)
         *    |
         *   macro-data segment header: 0xAA
         * 
         * 
         * event segment pattern:
         *   [.][.][..][....][.][ ... ]
         *    |  |  |    |4   |   | n
         *    |  |  |    |    |   |
         *    |  |  |    |    |  string key name (array of char) (char = 1 byte)
         *    |  |  |    |    |
         *    |  |  |    |   string length (byte)
         *    |  |  |    |
         *    |  |  |   key: bVK (Int32)
         *    |  |  |
         *    |  | delay value (Int16)
         *    |  |
         *    | event type (byte)
         *    |
         *   event segment header: 0xBB
         * 
         */
    }

    struct Key
    {
        public string str;
        private byte scan;
        private int vk; // extended (int)

        public int bVk { get { return vk; } }
        public byte bScan { get { return scan; } }
        
        public Key(int bVk, string str)
        {
            vk = 0;
            scan = 0;
            this.str = "";
            Set(bVk, str);
        }
        public void Set(int bVk, string str)
        {
            if (bVk < 0) return;
            if (bVk > 255)
                if (bVk != Mouse.WheelDown && bVk != Mouse.WheelUp &&
                bVk != Mouse.Left && bVk != Mouse.Right &&
                bVk != Mouse.XButton1 && bVk != Mouse.XButton2 &&
                bVk != Mouse.Middle) return;

            vk = bVk;
            this.str = str;
            scan = (byte)SF.MapVirtualKey((uint)vk, 0);
        }

        public static class Mouse
        {
            public const int WheelDown = 63885;
            public const int WheelUp = 63886;
            public const int Middle = 63891;
            public const int Right = 63892;
            public const int Left = 63893;
            public const int XButton1 = 63834;
            public const int XButton2 = 63835;
        }

        public void KeyDown()
        {
            if (vk == Mouse.Left) SF.mouse_event(SF.MouseEvent.LeftDown, 0, 0, 0, (UIntPtr)0);
            else if (vk == Mouse.Right) SF.mouse_event(SF.MouseEvent.RightDown, 0, 0, 0, (UIntPtr)0);
            else if (vk == Mouse.Middle) SF.mouse_event(SF.MouseEvent.MiddleDown, 0, 0, 0, (UIntPtr)0);
            else if (vk > 0 && vk < 256)
            {
                if (KeyHandler.useVirtualKeyCodes == true)
                    SF.keybd_event((byte)vk, scan, SF.KeyEvent.KeyDown, 0);
                else SF.keybd_event(0, scan, SF.KeyEvent.KeyDown, 0);
            }
            else {
                MessageBox.Show("Key.KeyDown Error!");
                return;
            }
        }

        public void KeyUp()
        {
            if (vk == Mouse.Left) SF.mouse_event(SF.MouseEvent.LeftUp, 0, 0, 0, (UIntPtr)0);
            else if (vk == Mouse.Right) SF.mouse_event(SF.MouseEvent.RightUp, 0, 0, 0, (UIntPtr)0);
            else if (vk == Mouse.Middle) SF.mouse_event(SF.MouseEvent.MiddleUp, 0, 0, 0, (UIntPtr)0);
            else if (vk > 0 && vk < 256)
            {
                if (KeyHandler.useVirtualKeyCodes == true)
                    SF.keybd_event((byte)vk, scan, SF.KeyEvent.KeyUp, 0);
                else SF.keybd_event(0, scan, SF.KeyEvent.KeyUp, 0);
            }
            else
            {
                MessageBox.Show("Key.KeyUp Error!");
                return;
            }
        }
    }

    class Macro
    {
        public class Event
        {
            private byte type;
            private Int16 delay;
            public WFApp.Key key;

            public byte Type
            {
                get { return type; }
                set
                {
                    if (value == KeyDown) type = KeyDown;
                    if (value == KeyUp) type = KeyUp;
                    if (value == Delay) type = Delay;
                }
            }
            public Int16 DelayValue
            {
                get { return delay; }
                set { if (type == Delay) delay = value; }
            }

            public const byte KeyDown = 1;
            public const byte KeyUp = 2;
            public const byte Delay = 0;

            public void Write(FileStream fs)
            {
                // wxMacro IO standart #1
                IO.Write.Byte(fs, 0xBB); // separator
                IO.Write.Byte(fs, type);
                IO.Write.Int16(fs, delay);
                IO.Write.Int32(fs, key.bVk);
                IO.Write.String(fs, key.str);
            }

            public static Event Read(FileStream fs)
            {
                // wxMacro IO standart #1
                Event ev = new Event();
                IO.Read.Byte(fs); // separator
                ev.Type = IO.Read.Byte(fs);
                ev.DelayValue = IO.Read.Int16(fs);
                int vk = IO.Read.Int32(fs);
                string str = IO.Read.String(fs);
                ev.key = new Key(vk, str);
                return ev;
            }
        }

        public class DataCounter
        {
            private int value;
            public int Value { get { return value; } }
            public void Inc() { value += 1; }
            public void Reset() { value = 0; }
        }

        public static class Method
        {
            // exact match the fActivationMethod's collection
            public const byte Hold = 0; // Need to hold button
            public const byte Switch = 1; // Button as trigger
        }

        private string name;
        public bool repeat;
        public bool active;
        public bool enabled;
        public bool finished;
        private byte actMethod;
        public float delayMultiplier;
        public bool preventTermination;

        public Stopwatch timer = new Stopwatch();
        public WFApp.Key actKey = new WFApp.Key();
        public DataCounter dataCounter = new DataCounter();
        public List<Macro.Event> data = new List<Macro.Event>();

        // protection from the sticky keys
        public List<WFApp.Key> keysPressed = new List<WFApp.Key>();

        public string Name
        {
            get { return name; }
            set {
                if (value.Length > 255) return;
                else name = value;
            }
        }
        public int ActMethod
        {
            get { return actMethod; }
            set
            {
                if (value == Method.Hold) actMethod = Method.Hold;
                if (value == Method.Switch) actMethod = Method.Switch;
            }
        }

        public Macro(string name)
        {
            if (name.Length < 255) this.name = name;
            else this.name = name.Substring(0, 255);

            // default
            repeat = false;
            active = false;
            enabled = false;
            finished = false;
            dataCounter.Reset();
            delayMultiplier = 1.0f;
            actMethod = Method.Hold;
            preventTermination = false;
            actKey.Set(189, "OemMinus");
            keysPressed.Clear();
            timer.Reset();
            data.Clear();
        }

        public void Write(FileStream fs)
        {
            // wxMacro IO standart #1
            IO.Write.Byte(fs, 0xAA); // separator
            IO.Write.Bool(fs, repeat);
            IO.Write.Bool(fs, preventTermination);
            IO.Write.Float(fs, delayMultiplier);
            IO.Write.String(fs, name);
            IO.Write.Byte(fs, actMethod);
            IO.Write.Int32(fs, actKey.bVk);
            IO.Write.String(fs, actKey.str);
            byte var; if (data.Count > 255) var = 255;
            else var = (byte)data.Count; IO.Write.Byte(fs, var);
            for (int i = 0; i < var; i++) data[i].Write(fs);
        }

        public static Macro Read(FileStream fs)
        {
            // wxMacro IO standart #1
            Macro mcr = new Macro("reading...");
            IO.Read.Byte(fs); // separator
            mcr.repeat = IO.Read.Bool(fs);
            mcr.preventTermination = IO.Read.Bool(fs);
            mcr.delayMultiplier = IO.Read.Float(fs);
            mcr.Name = IO.Read.String(fs);
            mcr.ActMethod = IO.Read.Byte(fs);
            string str; int vk;
            vk = IO.Read.Int32(fs);
            str = IO.Read.String(fs);
            mcr.actKey.Set(vk, str);
            int dc = IO.Read.Byte(fs);
            for (int i = 0; i < dc; i++)
                mcr.data.Add(Event.Read(fs));
            return mcr;
        }
    }

    static class Program
    {
        public static List<Macro> mainMacroCollection = new List<Macro>();
        public static int selectedMacroIndex;

        [STAThread]
        static void Main()
        {
            Thread t = new Thread(KeyHandler.TF);
            mainMacroCollection.Clear();
            selectedMacroIndex = -1;
            Open();

            t.Start();
            KeyHandler.Subscribe();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindowCtr());
            //KeyHandler.Unsubscribe(); // on form close
            t.Abort();
        }

        public static void Save()
        {
            FileStream fs = null;
            try { fs = File.Create(IO.FileNames.Database); }
            catch (UnauthorizedAccessException e)
            {
                // У вызывающего объекта отсутствует необходимое разрешение.
                // Параметр path указывает файл, доступный только для чтения.
                MessageBox.Show("Данные не были сохранены!\n" +
                    "Вызвано исключение File.Create:UnauthorizedAccessException\n\n" +
                    "У вызывающего объекта отсутствует необходимое разрешение.");
                return;
            }
            catch (DirectoryNotFoundException e)
            {
                // Указан недопустимый путь (например, он ведет на несопоставленный диск).
                MessageBox.Show("Данные не будут сохранены.\n" +
                    "Вызвано исключение File.Create:DirectoryNotFoundException\n\n" +
                    "Указан недостижимый путь.");
                return;
            }
            catch (IOException e)
            {
                // Ошибка ввода-вывода при создании файла.
                MessageBox.Show("Данные не будут сохранены.\n" +
                    "Вызвано исключение File.Create:IOException\n\n" +
                    "Ошибка ввода-вывода при создании файла.");
                return;
            }

            IO.Write.Std(fs); // current wxMacro IO standart
            byte nrec; if (mainMacroCollection.Count > 255) nrec = 255;
            else nrec = (byte)mainMacroCollection.Count; IO.Write.Byte(fs, nrec);
            for (int i = 0; i < nrec; i++) mainMacroCollection[i].Write(fs);

            fs.Close();
        }

        public static void Open()
        {
            FileStream fs = null;
            try { fs = File.Open(IO.FileNames.Database, FileMode.Open); }
            catch (FileNotFoundException e)
            {
                // Файл, заданный параметром path, не найден.
                MessageBox.Show("Вызвано исключение File.Open:FileNotFoundException\n\n" +
                    "Файл, заданный параметром path, не найден.");
                return;
            }
            catch (UnauthorizedAccessException e)
            {
                // Параметр path указывает файл, доступный только для чтения.
                // У вызывающего объекта отсутствует необходимое разрешение.
                MessageBox.Show("Вызвано исключение File.Open:UnauthorizedAccessException\n\n" +
                    "Параметр path указывает файл, доступный только для чтения или\n" +
                    "у вызывающего объекта отсутствует необходимое разрешение.");
                return;
            }
            catch (DirectoryNotFoundException e)
            {
                // Указан недопустимый путь (например, он ведет на несопоставленный диск).
                MessageBox.Show("Вызвано исключение File.Open:DirectoryNotFoundException\n\n" +
                    "Указан недостижимый путь.");
                return;
            }
            catch (IOException e)
            {
                // При открытии файла произошла ошибка ввода-вывода.
                MessageBox.Show("Вызвано исключение File.Open:IOException\n\n" +
                    "При открытии файла произошла ошибка ввода-вывода.");
                return;
            }

            byte std = IO.Read.Std(fs);
            if (std == 0) MessageBox.Show("Файл сохранения не прошел проверку.\nСудя по всему, данные были повреждены.");
            else if (std == IO.std_1)
            {
                mainMacroCollection.Clear();
                byte nrec = IO.Read.Byte(fs);
                for (int i = 0; i < nrec; i++)
                    mainMacroCollection.Add(Macro.Read(fs));
            }

            fs.Close();
        }

        static void TFOLD()
        {
            /*
            while (true)
            {
                tf_timer.Start();

                if (mainMacroCollection.Count <= 0) continue;

                for (int i = 0; i < enabledIndexList.Count; i++)
                {
                    byte bVk_prop = 0;
                    int dc = mainMacroCollection[enabledIndexList[i]].data_counter;
                    if (useVirtualKeyCodes == true) bVk_prop = mainMacroCollection[enabledIndexList[i]].data_old[dc].key.bVk;

                    // activation handler
                    if (mainMacroCollection[enabledIndexList[i]].actMethod == Macro.ACTM_HOLD)
                    {
                        SF.GetAsyncKeyState(mainMacroCollection[enabledIndexList[i]].actKey.bVk);
                        if (SF.GetAsyncKeyState(mainMacroCollection[enabledIndexList[i]].actKey.bVk) != 0) {
                            if (mainMacroCollection[enabledIndexList[i]].active != true)
                                mainMacroCollection[enabledIndexList[i]].active = true;
                        }
                        else if (mainMacroCollection[enabledIndexList[i]].active == true)
                        {
                            if (mainMacroCollection[enabledIndexList[i]].data_old[dc].key_down == true)
                                SF.keybd_event(bVk_prop, mainMacroCollection[enabledIndexList[i]].data_old[dc].key.bScan, SF.KEYEVENTF_KEYUP, 0); // key up
                            mainMacroCollection[enabledIndexList[i]].active = false;
                            mainMacroCollection[enabledIndexList[i]].finished = false;
                            mainMacroCollection[enabledIndexList[i]].data_counter = 0;
                            mainMacroCollection[enabledIndexList[i]].timer.Stop();
                            mainMacroCollection[enabledIndexList[i]].timer.Reset();
                            var tmp = mainMacroCollection[enabledIndexList[i]].data_old[dc];
                            tmp.key_down = false; tmp.pressed = false;
                            mainMacroCollection[enabledIndexList[i]].data_old[dc] = tmp;
                        }
                    }
                    else if (mainMacroCollection[enabledIndexList[i]].actMethod == Macro.ACTM_SWITCH)
                    {
                        SF.GetAsyncKeyState(mainMacroCollection[enabledIndexList[i]].actKey.bVk);
                        if (SF.GetAsyncKeyState(mainMacroCollection[enabledIndexList[i]].actKey.bVk) != 0)
                        {
                            // activation key: manual key up
                            if (useVirtualKeyCodes == true)
                                SF.keybd_event(mainMacroCollection[enabledIndexList[i]].actKey.bVk,
                                    mainMacroCollection[enabledIndexList[i]].actKey.bScan, SF.KEYEVENTF_KEYUP, 0);
                            else SF.keybd_event(0, mainMacroCollection[enabledIndexList[i]].actKey.bScan, SF.KEYEVENTF_KEYUP, 0);

                            if (mainMacroCollection[enabledIndexList[i]].active == true &&
                                mainMacroCollection[enabledIndexList[i]].terminate_emb == false)
                            {
                                if (mainMacroCollection[enabledIndexList[i]].data_old[dc].key_down == true)
                                    SF.keybd_event(bVk_prop, mainMacroCollection[enabledIndexList[i]].data_old[dc].key.bScan, SF.KEYEVENTF_KEYUP, 0); // key up
                                mainMacroCollection[enabledIndexList[i]].active = false;
                                mainMacroCollection[enabledIndexList[i]].finished = false;
                                mainMacroCollection[enabledIndexList[i]].data_counter = 0;
                                mainMacroCollection[enabledIndexList[i]].timer.Stop();
                                mainMacroCollection[enabledIndexList[i]].timer.Reset();
                                var tmp = mainMacroCollection[enabledIndexList[i]].data_old[dc];
                                tmp.key_down = false; tmp.pressed = false;
                                mainMacroCollection[enabledIndexList[i]].data_old[dc] = tmp;
                            }
                            else if (mainMacroCollection[enabledIndexList[i]].active == false)
                                mainMacroCollection[enabledIndexList[i]].active = true;
                        }
                    }
                    
                    // kbd emulation handler
                    if (mainMacroCollection[enabledIndexList[i]].active == true)
                    {
                        if (mainMacroCollection[enabledIndexList[i]].finished == false)
                        {
                            //int dc = mainMacroCollection[enabledIndexList[i]].data_counter; // to top

                            // before pressing
                            if (mainMacroCollection[enabledIndexList[i]].data_old[dc].pressed == false &&
                                mainMacroCollection[enabledIndexList[i]].data_old[dc].key_down == false)
                            {
                                if (mainMacroCollection[enabledIndexList[i]].data_old[dc].key.bVk == Key.VK_LBUTTON)
                                    SF.mouse_event(SF.MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
                                else SF.keybd_event(bVk_prop, mainMacroCollection[enabledIndexList[i]].data_old[dc].key.bScan, 0, 0); // key down
                                mainMacroCollection[enabledIndexList[i]].timer.Start();
                                // setting data.key_down
                                var tmp = mainMacroCollection[enabledIndexList[i]].data_old[dc];
                                tmp.key_down = true;
                                mainMacroCollection[enabledIndexList[i]].data_old[dc] = tmp;
                            }
                            // key down state
                            else if (mainMacroCollection[enabledIndexList[i]].data_old[dc].pressed == false &&
                                mainMacroCollection[enabledIndexList[i]].data_old[dc].key_down == true)
                            {
                                if (mainMacroCollection[enabledIndexList[i]].timer.ElapsedMilliseconds >=
                                    mainMacroCollection[enabledIndexList[i]].data_old[dc].duration)
                                {
                                    if (mainMacroCollection[enabledIndexList[i]].data_old[dc].key.bVk == Key.VK_LBUTTON)
                                        SF.mouse_event(SF.MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
                                    else SF.keybd_event(bVk_prop, mainMacroCollection[enabledIndexList[i]].data_old[dc].key.bScan, SF.KEYEVENTF_KEYUP, 0); // key up
                                    mainMacroCollection[enabledIndexList[i]].timer.Restart();
                                    // setting data.pressed
                                    var tmp = mainMacroCollection[enabledIndexList[i]].data_old[dc];
                                    tmp.pressed = true; tmp.key_down = false;
                                    mainMacroCollection[enabledIndexList[i]].data_old[dc] = tmp;
                                }
                            }
                            // after pressing (delay)
                            else if (mainMacroCollection[enabledIndexList[i]].data_old[dc].pressed == true &&
                                mainMacroCollection[enabledIndexList[i]].data_old[dc].key_down == false)
                            {
                                // end atom
                                if (mainMacroCollection[enabledIndexList[i]].timer.ElapsedMilliseconds >=
                                    mainMacroCollection[enabledIndexList[i]].data_old[dc].delay)
                                {
                                    var tmp = mainMacroCollection[enabledIndexList[i]].data_old[dc];
                                    tmp.key_down = false; tmp.pressed = false;
                                    mainMacroCollection[enabledIndexList[i]].data_old[dc] = tmp;
                                    mainMacroCollection[enabledIndexList[i]].data_counter += 1;
                                    // end data
                                    if (mainMacroCollection[enabledIndexList[i]].data_counter >=
                                        mainMacroCollection[enabledIndexList[i]].data_old.Count)
                                    {
                                        mainMacroCollection[enabledIndexList[i]].data_counter = 0;
                                        if (mainMacroCollection[enabledIndexList[i]].repeat == false)
                                        {
                                            mainMacroCollection[enabledIndexList[i]].timer.Stop();
                                            mainMacroCollection[enabledIndexList[i]].timer.Reset();
                                            mainMacroCollection[enabledIndexList[i]].finished = true;
                                            if (mainMacroCollection[enabledIndexList[i]].actMethod == Macro.ACTM_SWITCH)
                                            {
                                                // auto disable
                                                mainMacroCollection[enabledIndexList[i]].active = false;
                                                mainMacroCollection[enabledIndexList[i]].finished = false;
                                                mainMacroCollection[enabledIndexList[i]].data_counter = 0;
                                                var tmp2 = mainMacroCollection[enabledIndexList[i]].data_old[dc];
                                                tmp2.key_down = false; tmp.pressed = false;
                                                mainMacroCollection[enabledIndexList[i]].data_old[dc] = tmp2;
                                            }
                                        }
                                    }
                                }
                            }
                            // error check
                            if (mainMacroCollection[enabledIndexList[i]].data_old[dc].pressed == true &&
                                mainMacroCollection[enabledIndexList[i]].data_old[dc].key_down == true)
                            {
                                // WRONG !
                            }
                        }
                    }
                }

                // signals handling
                if (disableSignals.Count > 0) {
                    for (int i = 0; i < disableSignals.Count; i++)
                        enabledIndexList.Remove(disableSignals[i]);
                    disableSignals.Clear();
                }
                if (enableSignals.Count > 0) {
                    for (int i = 0; i < enableSignals.Count; i++)
                        enabledIndexList.Add(enableSignals[i]);
                    enableSignals.Clear();
                }

                tf_timer.Stop();
                tf_timer_ticks = tf_timer.ElapsedTicks;
                tf_timer_ms = tf_timer.ElapsedMilliseconds;
                tf_timer.Reset();
            }
            */
        }
    }

    static class KeyHandler
    {
        public class Timer
        {
            private int ms_loc;
            private int ticks;

            public int ms { get { return ms_loc; } }
            public int Ticks { get { return ticks; } }

            public Stopwatch sw = new Stopwatch();

            public void UpdateData()
            {
                ticks = (int)sw.ElapsedTicks;
                ms_loc = (int)sw.ElapsedMilliseconds;
                //sw.Restart();
            }
        }

        private static IKeyboardMouseEvents m_GlobalHook;

        public static List<int> enableSignals = new List<int>();
        public static List<int> disableSignals = new List<int>();
        public static List<int> enabledIndexList = new List<int>();

        public static Timer tf_timer = new Timer();
        public static Timer hk_timer = new Timer();
        public static Timer hm_timer = new Timer();
        private static Timer mw_timer = new Timer();

        public static bool useVirtualKeyCodes = true;
        public static bool ignoreMouseButtonsLR = true;
        public static int mw_timeout = 200;

        public static void TFOLD()
        {
            /*
            while (true)
            {
                timer.UpdateData();

                if (Program.mainMacroCollection.Count <= 0) { Thread.Sleep(500); continue; }

                for (int i = 0; i < enabledIndexList.Count; i++)
                {
                    int dc = Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Value;

                    // activation handler
                    if (Program.mainMacroCollection[enabledIndexList[i]].ActMethod == Macro.Method.Hold)
                    {
                        SF.GetAsyncKeyState(Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk);
                        if (SF.GetAsyncKeyState(Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk) != 0)
                        {
                            if (Program.mainMacroCollection[enabledIndexList[i]].active != true)
                                Program.mainMacroCollection[enabledIndexList[i]].active = true;
                        }
                        else if (Program.mainMacroCollection[enabledIndexList[i]].active == true)
                        {
                            // protection from the sticky keys
                            if (Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Count > 0)
                            {
                                for (int j = 0; j < Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Count; j++)
                                {
                                    if (Program.mainMacroCollection[enabledIndexList[i]].keysPressed[j].bVk == WFApp.Key.VK_LBUTTON)
                                        SF.mouse_event(SF.MouseEvent.LeftUp, 0, 0, 0, (UIntPtr)0);
                                    else if (Program.mainMacroCollection[enabledIndexList[i]].keysPressed[j].bVk == WFApp.Key.VK_RBUTTON)
                                        SF.mouse_event(SF.MouseEvent.RightUp, 0, 0, 0, (UIntPtr)0);
                                    else // regular key
                                    {
                                        if (useVirtualKeyCodes == true)
                                        {
                                            SF.keybd_event(Program.mainMacroCollection[enabledIndexList[i]].keysPressed[j].bVk,
                                                Program.mainMacroCollection[enabledIndexList[i]].keysPressed[j].bScan, SF.KeyEvent.KeyUp, 0);
                                        }
                                        else SF.keybd_event(0, Program.mainMacroCollection[enabledIndexList[i]].keysPressed[j].bScan, SF.KeyEvent.KeyUp, 0);
                                    }
                                }
                            }
                            Program.mainMacroCollection[enabledIndexList[i]].active = false;
                            Program.mainMacroCollection[enabledIndexList[i]].finished = false;
                            Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Reset();
                            Program.mainMacroCollection[enabledIndexList[i]].timer.Reset();
                        }
                    }
                    else if (Program.mainMacroCollection[enabledIndexList[i]].ActMethod == Macro.Method.Switch)
                    {
                        SF.GetAsyncKeyState(Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk);
                        if (SF.GetAsyncKeyState(Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk) != 0 ||
                            (Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk == Key.VK_LBUTTON && MouseLeft == true) ||
                             (Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk == Key.VK_RBUTTON && MouseRight == true))
                        {
                            // activation key: manual key up
                            if (MouseLeft == false && MouseRight == false)
                            {
                                if (useVirtualKeyCodes == true)
                                {
                                    SF.keybd_event(Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk,
                                        Program.mainMacroCollection[enabledIndexList[i]].actKey.bScan, SF.KeyEvent.KeyUp, 0);
                                }
                                else SF.keybd_event(0, Program.mainMacroCollection[enabledIndexList[i]].actKey.bScan, SF.KeyEvent.KeyUp, 0);
                            }

                            MouseLeft = false; MouseRight = false;

                            if (Program.mainMacroCollection[enabledIndexList[i]].active == true)
                            {
                                if (Program.mainMacroCollection[enabledIndexList[i]].repeat != false ||
                                    Program.mainMacroCollection[enabledIndexList[i]].preventTermination == false)
                                {
                                    // protection from the sticky keys
                                    if (Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Count > 0)
                                    {
                                        for (int j = 0; j < Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Count; j++)
                                        {
                                            if (Program.mainMacroCollection[enabledIndexList[i]].keysPressed[j].bVk == WFApp.Key.VK_LBUTTON)
                                                SF.mouse_event(SF.MouseEvent.LeftUp, 0, 0, 0, (UIntPtr)0);
                                            else if (Program.mainMacroCollection[enabledIndexList[i]].keysPressed[j].bVk == WFApp.Key.VK_RBUTTON)
                                                SF.mouse_event(SF.MouseEvent.RightUp, 0, 0, 0, (UIntPtr)0);
                                            else // regular key
                                            {
                                                if (useVirtualKeyCodes == true)
                                                {
                                                    SF.keybd_event(Program.mainMacroCollection[enabledIndexList[i]].keysPressed[j].bVk,
                                                        Program.mainMacroCollection[enabledIndexList[i]].keysPressed[j].bScan, SF.KeyEvent.KeyUp, 0);
                                                }
                                                else SF.keybd_event(0, Program.mainMacroCollection[enabledIndexList[i]].keysPressed[j].bScan, SF.KeyEvent.KeyUp, 0);
                                            }
                                        }
                                    }
                                    Program.mainMacroCollection[enabledIndexList[i]].active = false;
                                    Program.mainMacroCollection[enabledIndexList[i]].finished = false;
                                    Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Reset();
                                    Program.mainMacroCollection[enabledIndexList[i]].timer.Reset();
                                }
                            }
                            else if (Program.mainMacroCollection[enabledIndexList[i]].active == false)
                                Program.mainMacroCollection[enabledIndexList[i]].active = true;
                        }
                    }

                    //-------------------------------------------------------------------------------------

                    // kbd emulation handler
                    if (Program.mainMacroCollection[enabledIndexList[i]].active == true &&
                        Program.mainMacroCollection[enabledIndexList[i]].finished == false)
                    {
                        //......................
                        if (Program.mainMacroCollection[enabledIndexList[i]].data[dc].Type == Macro.Event.KeyDown)
                        {
                            Program.mainMacroCollection[enabledIndexList[i]].data[dc].key.KeyDown();
                            Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Add(
                                Program.mainMacroCollection[enabledIndexList[i]].data[dc].key);
                            Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Inc();
                            Program.mainMacroCollection[enabledIndexList[i]].timer.Restart();
                        }
                        else if (Program.mainMacroCollection[enabledIndexList[i]].data[dc].Type == Macro.Event.KeyUp)
                        {
                            Program.mainMacroCollection[enabledIndexList[i]].data[dc].key.KeyUp();
                            Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Remove(
                                Program.mainMacroCollection[enabledIndexList[i]].data[dc].key);
                            Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Inc();
                            Program.mainMacroCollection[enabledIndexList[i]].timer.Restart();
                        }
                        else if (Program.mainMacroCollection[enabledIndexList[i]].data[dc].Type == Macro.Event.Delay)
                        {
                            if (Program.mainMacroCollection[enabledIndexList[i]].timer.ElapsedMilliseconds >=
                                (Program.mainMacroCollection[enabledIndexList[i]].data[dc].DelayValue *
                                Program.mainMacroCollection[enabledIndexList[i]].delayMultiplier))
                            {
                                Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Inc();
                                Program.mainMacroCollection[enabledIndexList[i]].timer.Restart();
                            }
                        }
                        // end data: repeat check
                        if (Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Value >=
                            Program.mainMacroCollection[enabledIndexList[i]].data.Count)
                        {
                            if (Program.mainMacroCollection[enabledIndexList[i]].repeat == false)
                            {
                                // auto deactivation
                                Program.mainMacroCollection[enabledIndexList[i]].finished = true;
                                if (Program.mainMacroCollection[enabledIndexList[i]].ActMethod == Macro.Method.Switch)
                                {
                                    Program.mainMacroCollection[enabledIndexList[i]].active = false;
                                    Program.mainMacroCollection[enabledIndexList[i]].finished = false;
                                    Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Reset();
                                    Program.mainMacroCollection[enabledIndexList[i]].timer.Reset();
                                    // protection from the sticky keys
                                    if (Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Count != 0)
                                    {
                                        for (int j = 0; j < Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Count; j++)
                                            Program.mainMacroCollection[enabledIndexList[i]].keysPressed[j].KeyUp();
                                    }
                                }
                            }
                            else Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Reset();
                        }
                    }
                }

                // signals handling
                if (enableSignals.Count > 0)
                {
                    for (int i = 0; i < enableSignals.Count; i++)
                        enabledIndexList.Add(enableSignals[i]);
                    enableSignals.Clear();
                }
                if (disableSignals.Count > 0)
                {
                    for (int i = 0; i < disableSignals.Count; i++)
                        enabledIndexList.Remove(disableSignals[i]);
                    disableSignals.Clear();
                }
            }
            */
        }

        public static void TF()
        {
            while (true)
            {
                tf_timer.UpdateData(); tf_timer.sw.Restart();
                if (Program.mainMacroCollection.Count <= 0) { Thread.Sleep(500); continue; }

                for (int i = 0; i < enabledIndexList.Count; i++)
                {
                    int dc = Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Value;

                    // kbd emulation handler
                    if (Program.mainMacroCollection[enabledIndexList[i]].active == true &&
                        Program.mainMacroCollection[enabledIndexList[i]].finished == false)
                    {
                        //.......................
                        if (Program.mainMacroCollection[enabledIndexList[i]].data[dc].Type == Macro.Event.KeyDown)
                        {
                            Program.mainMacroCollection[enabledIndexList[i]].data[dc].key.KeyDown();
                            Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Add(
                                Program.mainMacroCollection[enabledIndexList[i]].data[dc].key);
                            Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Inc();
                            Program.mainMacroCollection[enabledIndexList[i]].timer.Restart();
                        }
                        else if (Program.mainMacroCollection[enabledIndexList[i]].data[dc].Type == Macro.Event.KeyUp)
                        {
                            Program.mainMacroCollection[enabledIndexList[i]].data[dc].key.KeyUp();
                            Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Remove(
                                Program.mainMacroCollection[enabledIndexList[i]].data[dc].key);
                            Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Inc();
                            Program.mainMacroCollection[enabledIndexList[i]].timer.Restart();
                        }
                        else if (Program.mainMacroCollection[enabledIndexList[i]].data[dc].Type == Macro.Event.Delay)
                        {
                            if (Program.mainMacroCollection[enabledIndexList[i]].timer.ElapsedMilliseconds >=
                                (Program.mainMacroCollection[enabledIndexList[i]].data[dc].DelayValue *
                                Program.mainMacroCollection[enabledIndexList[i]].delayMultiplier))
                            {
                                Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Inc();
                                Program.mainMacroCollection[enabledIndexList[i]].timer.Restart();
                            }
                        }
                        // end data: repeat check
                        if (Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Value >=
                            Program.mainMacroCollection[enabledIndexList[i]].data.Count)
                        {
                            if (Program.mainMacroCollection[enabledIndexList[i]].repeat == false)
                            {
                                // auto deactivation
                                Program.mainMacroCollection[enabledIndexList[i]].finished = true;
                                if (Program.mainMacroCollection[enabledIndexList[i]].ActMethod == Macro.Method.Switch)
                                {
                                    Program.mainMacroCollection[enabledIndexList[i]].active = false;
                                    Program.mainMacroCollection[enabledIndexList[i]].finished = false;
                                    Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Reset();
                                    Program.mainMacroCollection[enabledIndexList[i]].timer.Reset();
                                    // protection from the sticky keys
                                    if (Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Count != 0)
                                    {
                                        for (int j = 0; j < Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Count; j++)
                                            Program.mainMacroCollection[enabledIndexList[i]].keysPressed[j].KeyUp();
                                    }
                                }
                            }
                            else Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Reset();
                        }
                    }
                }

                // signals handling
                if (enableSignals.Count > 0)
                {
                    for (int i = 0; i < enableSignals.Count; i++)
                        enabledIndexList.Add(enableSignals[i]);
                    enableSignals.Clear();
                }
                if (disableSignals.Count > 0)
                {
                    for (int i = 0; i < disableSignals.Count; i++)
                        enabledIndexList.Remove(disableSignals[i]);
                    disableSignals.Clear();
                }
            }
        }

        //----------------------------------------

        public static void Subscribe()
        {
            m_GlobalHook = Hook.GlobalEvents();
            m_GlobalHook.KeyUp += GlobalHookKeyUp;
            m_GlobalHook.KeyDown += GlobalHookKeyDown;
            m_GlobalHook.MouseUpExt += GlobalHookMouseUpExt;
            m_GlobalHook.MouseDownExt += GlobalHookMouseDownExt;
            m_GlobalHook.MouseWheelExt += GlobalHookMouseWheelExt;
        }

        public static void Unsubscribe()
        {
            m_GlobalHook.KeyUp -= GlobalHookKeyUp;
            m_GlobalHook.KeyDown -= GlobalHookKeyDown;
            m_GlobalHook.MouseUpExt -= GlobalHookMouseUpExt;
            m_GlobalHook.MouseDownExt -= GlobalHookMouseDownExt;
            m_GlobalHook.MouseWheelExt -= GlobalHookMouseWheelExt;
            m_GlobalHook.Dispose();

            tf_timer.sw.Stop();
            hk_timer.sw.Stop();
            hm_timer.sw.Stop();
            mw_timer.sw.Stop();
        }

        public static void GlobalHookKeyDown(object sender, KeyEventArgs e)
        {
            hk_timer.sw.Start();
            for (int i = 0; i < enabledIndexList.Count; i++)
            {
                if (Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk == e.KeyValue)
                {
                    if (Program.mainMacroCollection[enabledIndexList[i]].ActMethod == Macro.Method.Hold)
                    {
                        if (Program.mainMacroCollection[enabledIndexList[i]].active != true)
                        {
                            Program.mainMacroCollection[enabledIndexList[i]].active = true;
                        }
                    }
                    else if (Program.mainMacroCollection[enabledIndexList[i]].ActMethod == Macro.Method.Switch)
                    {
                        SF.keybd_event((byte)e.KeyValue, (byte)SF.MapVirtualKey((uint)e.KeyValue, 0), SF.KeyEvent.KeyUp, 0);
                        if (Program.mainMacroCollection[enabledIndexList[i]].active == true)
                        {
                            if (Program.mainMacroCollection[enabledIndexList[i]].repeat != false ||
                                Program.mainMacroCollection[enabledIndexList[i]].preventTermination == false)
                            {
                                Program.mainMacroCollection[enabledIndexList[i]].active = false;
                                Program.mainMacroCollection[enabledIndexList[i]].finished = false;
                                Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Reset();
                                Program.mainMacroCollection[enabledIndexList[i]].timer.Reset();
                                // protection from the sticky keys
                                if (Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Count != 0)
                                {
                                    for (int j = 0; j < Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Count; j++)
                                        Program.mainMacroCollection[enabledIndexList[i]].keysPressed[j].KeyUp();
                                }
                            }
                        }
                        else Program.mainMacroCollection[enabledIndexList[i]].active = true;
                    }
                }
            }
            hk_timer.UpdateData();
            hk_timer.sw.Reset();
        }

        public static void GlobalHookKeyUp(object sender, KeyEventArgs e)
        {
            for (int i = 0; i < enabledIndexList.Count; i++)
            {
                if (Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk == e.KeyValue)
                {
                    if (Program.mainMacroCollection[enabledIndexList[i]].ActMethod == Macro.Method.Hold)
                    {
                        if (Program.mainMacroCollection[enabledIndexList[i]].active == true)
                        {
                            // Macro.Method.Hold: ignore Macro.preventTermination value
                            Program.mainMacroCollection[enabledIndexList[i]].active = false;
                            Program.mainMacroCollection[enabledIndexList[i]].finished = false;
                            Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Reset();
                            Program.mainMacroCollection[enabledIndexList[i]].timer.Reset();
                            // protection from the sticky keys
                            if (Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Count != 0)
                            {
                                for (int j = 0; j < Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Count; j++)
                                    Program.mainMacroCollection[enabledIndexList[i]].keysPressed[j].KeyUp();
                            }
                        }
                    }
                }
            }
        }

        // FOCUS ! ! !    HWND WINAPI GetForegroundWindow(void);

        private static void GlobalHookMouseDownExt(object sender, MouseEventExtArgs e)
        {
            hm_timer.sw.Start();
            for (int i = 0; i < enabledIndexList.Count; i++)
            {
                if ((Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk == Key.Mouse.Middle && e.Button == MouseButtons.Middle) ||
                    (Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk == Key.Mouse.XButton1 && e.Button == MouseButtons.XButton1) ||
                    (Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk == Key.Mouse.XButton2 && e.Button == MouseButtons.XButton2) ||
                    (Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk == Key.Mouse.Left && e.Button == MouseButtons.Left && ignoreMouseButtonsLR == false) ||
                    (Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk == Key.Mouse.Right && e.Button == MouseButtons.Right && ignoreMouseButtonsLR == false))
                {
                    if (Program.mainMacroCollection[enabledIndexList[i]].ActMethod == Macro.Method.Hold)
                    {
                        if (Program.mainMacroCollection[enabledIndexList[i]].active != true)
                            Program.mainMacroCollection[enabledIndexList[i]].active = true;
                    }
                    else if (Program.mainMacroCollection[enabledIndexList[i]].ActMethod == Macro.Method.Switch)
                    {
                        if (e.Button == MouseButtons.Left) SF.mouse_event(SF.MouseEvent.LeftUp, 0, 0, 0, (UIntPtr)0);
                        if (e.Button == MouseButtons.Right) SF.mouse_event(SF.MouseEvent.RightUp, 0, 0, 0, (UIntPtr)0);
                        if (e.Button == MouseButtons.Middle) SF.mouse_event(SF.MouseEvent.MiddleUp, 0, 0, 0, (UIntPtr)0);

                        if (Program.mainMacroCollection[enabledIndexList[i]].active == true)
                        {
                            if (Program.mainMacroCollection[enabledIndexList[i]].repeat != false ||
                                Program.mainMacroCollection[enabledIndexList[i]].preventTermination == false)
                            {
                                Program.mainMacroCollection[enabledIndexList[i]].active = false;
                                Program.mainMacroCollection[enabledIndexList[i]].finished = false;
                                Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Reset();
                                Program.mainMacroCollection[enabledIndexList[i]].timer.Reset();
                                // protection from the sticky keys
                                if (Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Count != 0)
                                {
                                    for (int j = 0; j < Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Count; j++)
                                        Program.mainMacroCollection[enabledIndexList[i]].keysPressed[j].KeyUp();
                                }
                            }
                        }
                        else Program.mainMacroCollection[enabledIndexList[i]].active = true;
                    }
                }
            }
            hm_timer.UpdateData();
            hm_timer.sw.Reset();
        }

        private static void GlobalHookMouseUpExt(object sender, MouseEventExtArgs e)
        {
            for (int i = 0; i < enabledIndexList.Count; i++)
            {
                if ((Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk == Key.Mouse.Middle && e.Button == MouseButtons.Middle) ||
                    (Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk == Key.Mouse.XButton1 && e.Button == MouseButtons.XButton1) ||
                    (Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk == Key.Mouse.XButton2 && e.Button == MouseButtons.XButton2) ||
                    (Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk == Key.Mouse.Left && e.Button == MouseButtons.Left && ignoreMouseButtonsLR == false) ||
                    (Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk == Key.Mouse.Right && e.Button == MouseButtons.Right && ignoreMouseButtonsLR == false))
                {
                    if (Program.mainMacroCollection[enabledIndexList[i]].ActMethod == Macro.Method.Hold)
                    {
                        if (Program.mainMacroCollection[enabledIndexList[i]].active == true)
                        {
                            // Macro.Method.Hold: ignore Macro.preventTermination value
                            Program.mainMacroCollection[enabledIndexList[i]].active = false;
                            Program.mainMacroCollection[enabledIndexList[i]].finished = false;
                            Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Reset();
                            Program.mainMacroCollection[enabledIndexList[i]].timer.Reset();
                            // protection from the sticky keys
                            if (Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Count != 0)
                            {
                                for (int j = 0; j < Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Count; j++)
                                    Program.mainMacroCollection[enabledIndexList[i]].keysPressed[j].KeyUp();
                            }
                        }
                    }
                }
            }
        }

        private static void GlobalHookMouseWheelExt(object sender, MouseEventExtArgs e)
        {
            // IsRunning check: WRONG
            if (mw_timer.sw.IsRunning == true && mw_timer.sw.ElapsedMilliseconds < mw_timeout) return;

            for (int i = 0; i < enabledIndexList.Count; i++)
            {
                if ((Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk == Key.Mouse.WheelUp && e.Delta > 0) ||
                    (Program.mainMacroCollection[enabledIndexList[i]].actKey.bVk == Key.Mouse.WheelDown && e.Delta < 0))
                {
                    if (Program.mainMacroCollection[enabledIndexList[i]].ActMethod == Macro.Method.Switch)
                    {
                        if (Program.mainMacroCollection[enabledIndexList[i]].active == true)
                        {
                            if (Program.mainMacroCollection[enabledIndexList[i]].repeat != false ||
                                Program.mainMacroCollection[enabledIndexList[i]].preventTermination == false)
                            {
                                Program.mainMacroCollection[enabledIndexList[i]].active = false;
                                Program.mainMacroCollection[enabledIndexList[i]].finished = false;
                                Program.mainMacroCollection[enabledIndexList[i]].dataCounter.Reset();
                                Program.mainMacroCollection[enabledIndexList[i]].timer.Reset();
                                // protection from the sticky keys
                                if (Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Count != 0)
                                {
                                    for (int j = 0; j < Program.mainMacroCollection[enabledIndexList[i]].keysPressed.Count; j++)
                                        Program.mainMacroCollection[enabledIndexList[i]].keysPressed[j].KeyUp();
                                }
                            }
                        }
                        else Program.mainMacroCollection[enabledIndexList[i]].active = true;
                    }
                }
            }

            mw_timer.sw.Restart();
        }
    }
}