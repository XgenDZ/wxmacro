﻿namespace WFApp
{
    partial class MainWindowCtr
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindowCtr));
            this.macroCollection = new System.Windows.Forms.ListBox();
            this.macroEnabled = new System.Windows.Forms.ListBox();
            this.macroData = new System.Windows.Forms.ListBox();
            this.bMacroDataDelete = new System.Windows.Forms.Button();
            this.bStartMacroRecord = new System.Windows.Forms.Button();
            this.bStopMacroRecord = new System.Windows.Forms.Button();
            this.bMacroEnable = new System.Windows.Forms.Button();
            this.bMacroDisable = new System.Windows.Forms.Button();
            this.group_macroCollection = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.group_macroConfig = new System.Windows.Forms.GroupBox();
            this.fAddEventMode = new System.Windows.Forms.ComboBox();
            this.bAddEvent = new System.Windows.Forms.Button();
            this.fAddEventSel = new System.Windows.Forms.ComboBox();
            this.fPreventTermination = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.bMacroDataClear = new System.Windows.Forms.Button();
            this.fEventDelay = new System.Windows.Forms.NumericUpDown();
            this.fEventDelayMultiplier = new System.Windows.Forms.NumericUpDown();
            this.fMacroRepeat = new System.Windows.Forms.CheckBox();
            this.fActivationMethod = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.bMacroActivationKeySet = new System.Windows.Forms.Button();
            this.fMacroActivationKey = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.fMacroName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.bNewMacro = new System.Windows.Forms.Button();
            this.bDeleteMacro = new System.Windows.Forms.Button();
            this.fExtendedButtonSelection = new System.Windows.Forms.CheckBox();
            this.statusStrip_mainMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statusStrip_Debug = new System.Windows.Forms.ToolStripDropDownButton();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debug_TFexecTime = new System.Windows.Forms.ToolStripMenuItem();
            this.debug_enabledIndexListAndSignals = new System.Windows.Forms.ToolStripMenuItem();
            this.debug_macroEnabled_indexList = new System.Windows.Forms.ToolStripMenuItem();
            this.debug_macroCollection_IndexList = new System.Windows.Forms.ToolStripMenuItem();
            this.debug_macroData = new System.Windows.Forms.ToolStripMenuItem();
            this.debug_mainMacroCollection = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.fDisableMainMouseBtn = new System.Windows.Forms.CheckBox();
            this.f_mwTimeout = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.fUseVirtualKeyCodes = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.group_macroCollection.SuspendLayout();
            this.group_macroConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fEventDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fEventDelayMultiplier)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.f_mwTimeout)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // macroCollection
            // 
            this.macroCollection.FormattingEnabled = true;
            this.macroCollection.Location = new System.Drawing.Point(9, 17);
            this.macroCollection.Name = "macroCollection";
            this.macroCollection.Size = new System.Drawing.Size(141, 160);
            this.macroCollection.TabIndex = 0;
            this.macroCollection.SelectedValueChanged += new System.EventHandler(this.macroCollection_SelectedValueChanged);
            // 
            // macroEnabled
            // 
            this.macroEnabled.FormattingEnabled = true;
            this.macroEnabled.Location = new System.Drawing.Point(9, 241);
            this.macroEnabled.Name = "macroEnabled";
            this.macroEnabled.Size = new System.Drawing.Size(141, 95);
            this.macroEnabled.TabIndex = 1;
            // 
            // macroData
            // 
            this.macroData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.macroData.FormattingEnabled = true;
            this.macroData.ItemHeight = 15;
            this.macroData.Items.AddRange(new object[] {
            "[button] \"T\"",
            "[delay] 1000 ms",
            "[button] \"A\"",
            "[delay] 1000 ms",
            "Press",
            "Delay",
            "Release",
            "\\/ \"T\"",
            "-- \"T\"",
            "/\\ \"T\"",
            "",
            "\\/   OemMinus",
            "-- 1234 ticks",
            "<  OemOpenBrackets  >  \\/-1000 ms-/\\"});
            this.macroData.Location = new System.Drawing.Point(9, 17);
            this.macroData.Name = "macroData";
            this.macroData.Size = new System.Drawing.Size(219, 349);
            this.macroData.TabIndex = 2;
            this.macroData.SelectedIndexChanged += new System.EventHandler(this.macroData_SelectedIndexChanged);
            // 
            // bMacroDataDelete
            // 
            this.bMacroDataDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bMacroDataDelete.Location = new System.Drawing.Point(12, 22);
            this.bMacroDataDelete.Name = "bMacroDataDelete";
            this.bMacroDataDelete.Size = new System.Drawing.Size(139, 23);
            this.bMacroDataDelete.TabIndex = 3;
            this.bMacroDataDelete.Text = "Удалить Элемент";
            this.bMacroDataDelete.UseVisualStyleBackColor = true;
            this.bMacroDataDelete.Click += new System.EventHandler(this.bMacroDataDelete_Click);
            // 
            // bStartMacroRecord
            // 
            this.bStartMacroRecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bStartMacroRecord.Location = new System.Drawing.Point(252, 17);
            this.bStartMacroRecord.Name = "bStartMacroRecord";
            this.bStartMacroRecord.Size = new System.Drawing.Size(130, 23);
            this.bStartMacroRecord.TabIndex = 4;
            this.bStartMacroRecord.Text = "Начать запись";
            this.bStartMacroRecord.UseVisualStyleBackColor = true;
            this.bStartMacroRecord.KeyDown += new System.Windows.Forms.KeyEventHandler(this.bStartMacroRecord_KeyDown);
            this.bStartMacroRecord.KeyUp += new System.Windows.Forms.KeyEventHandler(this.bStartMacroRecord_KeyUp);
            this.bStartMacroRecord.Leave += new System.EventHandler(this.bStartMacroRecord_Leave);
            // 
            // bStopMacroRecord
            // 
            this.bStopMacroRecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bStopMacroRecord.Location = new System.Drawing.Point(389, 17);
            this.bStopMacroRecord.Name = "bStopMacroRecord";
            this.bStopMacroRecord.Size = new System.Drawing.Size(130, 23);
            this.bStopMacroRecord.TabIndex = 5;
            this.bStopMacroRecord.Text = "Завершить запись";
            this.bStopMacroRecord.UseVisualStyleBackColor = true;
            this.bStopMacroRecord.Click += new System.EventHandler(this.bStopMacroRecord_Click);
            // 
            // bMacroEnable
            // 
            this.bMacroEnable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bMacroEnable.Location = new System.Drawing.Point(9, 183);
            this.bMacroEnable.Name = "bMacroEnable";
            this.bMacroEnable.Size = new System.Drawing.Size(141, 23);
            this.bMacroEnable.TabIndex = 6;
            this.bMacroEnable.Text = "Включить";
            this.bMacroEnable.UseVisualStyleBackColor = true;
            this.bMacroEnable.Click += new System.EventHandler(this.bMacroEnable_Click);
            // 
            // bMacroDisable
            // 
            this.bMacroDisable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bMacroDisable.Location = new System.Drawing.Point(8, 342);
            this.bMacroDisable.Name = "bMacroDisable";
            this.bMacroDisable.Size = new System.Drawing.Size(142, 23);
            this.bMacroDisable.TabIndex = 7;
            this.bMacroDisable.Text = "Отключить";
            this.bMacroDisable.UseVisualStyleBackColor = true;
            this.bMacroDisable.Click += new System.EventHandler(this.bMacroDisable_Click);
            // 
            // group_macroCollection
            // 
            this.group_macroCollection.Controls.Add(this.label1);
            this.group_macroCollection.Controls.Add(this.macroCollection);
            this.group_macroCollection.Controls.Add(this.bMacroDisable);
            this.group_macroCollection.Controls.Add(this.bMacroEnable);
            this.group_macroCollection.Controls.Add(this.macroEnabled);
            this.group_macroCollection.Location = new System.Drawing.Point(12, 12);
            this.group_macroCollection.Name = "group_macroCollection";
            this.group_macroCollection.Padding = new System.Windows.Forms.Padding(6, 1, 6, 6);
            this.group_macroCollection.Size = new System.Drawing.Size(159, 376);
            this.group_macroCollection.TabIndex = 8;
            this.group_macroCollection.TabStop = false;
            this.group_macroCollection.Text = "Коллекция Макросов";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 225);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Активные макросы";
            // 
            // group_macroConfig
            // 
            this.group_macroConfig.Controls.Add(this.groupBox2);
            this.group_macroConfig.Controls.Add(this.fPreventTermination);
            this.group_macroConfig.Controls.Add(this.label7);
            this.group_macroConfig.Controls.Add(this.fEventDelay);
            this.group_macroConfig.Controls.Add(this.fEventDelayMultiplier);
            this.group_macroConfig.Controls.Add(this.fMacroRepeat);
            this.group_macroConfig.Controls.Add(this.fActivationMethod);
            this.group_macroConfig.Controls.Add(this.label6);
            this.group_macroConfig.Controls.Add(this.bMacroActivationKeySet);
            this.group_macroConfig.Controls.Add(this.fMacroActivationKey);
            this.group_macroConfig.Controls.Add(this.label4);
            this.group_macroConfig.Controls.Add(this.label3);
            this.group_macroConfig.Controls.Add(this.macroData);
            this.group_macroConfig.Controls.Add(this.bStopMacroRecord);
            this.group_macroConfig.Controls.Add(this.fMacroName);
            this.group_macroConfig.Controls.Add(this.bStartMacroRecord);
            this.group_macroConfig.Controls.Add(this.label2);
            this.group_macroConfig.Location = new System.Drawing.Point(183, 12);
            this.group_macroConfig.Name = "group_macroConfig";
            this.group_macroConfig.Padding = new System.Windows.Forms.Padding(6, 1, 6, 6);
            this.group_macroConfig.Size = new System.Drawing.Size(527, 376);
            this.group_macroConfig.TabIndex = 9;
            this.group_macroConfig.TabStop = false;
            this.group_macroConfig.Text = "Конфигурация Макроса";
            // 
            // fAddEventMode
            // 
            this.fAddEventMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fAddEventMode.FormattingEnabled = true;
            this.fAddEventMode.Items.AddRange(new object[] {
            "A",
            "B"});
            this.fAddEventMode.Location = new System.Drawing.Point(77, 51);
            this.fAddEventMode.Name = "fAddEventMode";
            this.fAddEventMode.Size = new System.Drawing.Size(32, 23);
            this.fAddEventMode.TabIndex = 28;
            this.fAddEventMode.SelectedIndexChanged += new System.EventHandler(this.fAddEventMode_SelectedIndexChanged);
            this.fAddEventMode.SelectedValueChanged += new System.EventHandler(this.fAddEventMode_SelectedValueChanged);
            // 
            // bAddEvent
            // 
            this.bAddEvent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bAddEvent.Location = new System.Drawing.Point(115, 50);
            this.bAddEvent.Name = "bAddEvent";
            this.bAddEvent.Size = new System.Drawing.Size(133, 24);
            this.bAddEvent.TabIndex = 27;
            this.bAddEvent.Text = "Добавить элемент";
            this.bAddEvent.UseVisualStyleBackColor = true;
            this.bAddEvent.Click += new System.EventHandler(this.bAddEvent_Click);
            // 
            // fAddEventSel
            // 
            this.fAddEventSel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fAddEventSel.FormattingEnabled = true;
            this.fAddEventSel.Items.AddRange(new object[] {
            "Delay",
            "Button"});
            this.fAddEventSel.Location = new System.Drawing.Point(12, 51);
            this.fAddEventSel.Name = "fAddEventSel";
            this.fAddEventSel.Size = new System.Drawing.Size(59, 23);
            this.fAddEventSel.TabIndex = 26;
            this.fAddEventSel.SelectedIndexChanged += new System.EventHandler(this.fAddEventSel_SelectedIndexChanged);
            // 
            // fPreventTermination
            // 
            this.fPreventTermination.AutoSize = true;
            this.fPreventTermination.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fPreventTermination.Location = new System.Drawing.Point(252, 174);
            this.fPreventTermination.Name = "fPreventTermination";
            this.fPreventTermination.Size = new System.Drawing.Size(217, 19);
            this.fPreventTermination.TabIndex = 25;
            this.fPreventTermination.Text = "Запретить прерывание макроса";
            this.fPreventTermination.UseVisualStyleBackColor = true;
            this.fPreventTermination.CheckedChanged += new System.EventHandler(this.fPreventTermination_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(249, 201);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(178, 15);
            this.label7.TabIndex = 18;
            this.label7.Text = "Общий множитель задержки:";
            // 
            // bMacroDataClear
            // 
            this.bMacroDataClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bMacroDataClear.Location = new System.Drawing.Point(157, 22);
            this.bMacroDataClear.Name = "bMacroDataClear";
            this.bMacroDataClear.Size = new System.Drawing.Size(91, 23);
            this.bMacroDataClear.TabIndex = 10;
            this.bMacroDataClear.Text = "Очистить";
            this.bMacroDataClear.UseVisualStyleBackColor = true;
            this.bMacroDataClear.Click += new System.EventHandler(this.bMacroDataClear_Click);
            // 
            // fEventDelay
            // 
            this.fEventDelay.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fEventDelay.Location = new System.Drawing.Point(320, 225);
            this.fEventDelay.Margin = new System.Windows.Forms.Padding(0);
            this.fEventDelay.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.fEventDelay.Name = "fEventDelay";
            this.fEventDelay.Size = new System.Drawing.Size(58, 23);
            this.fEventDelay.TabIndex = 23;
            this.fEventDelay.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.fEventDelay.ValueChanged += new System.EventHandler(this.fEventDelay_ValueChanged);
            // 
            // fEventDelayMultiplier
            // 
            this.fEventDelayMultiplier.DecimalPlaces = 2;
            this.fEventDelayMultiplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fEventDelayMultiplier.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.fEventDelayMultiplier.Location = new System.Drawing.Point(430, 199);
            this.fEventDelayMultiplier.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.fEventDelayMultiplier.Name = "fEventDelayMultiplier";
            this.fEventDelayMultiplier.Size = new System.Drawing.Size(46, 21);
            this.fEventDelayMultiplier.TabIndex = 24;
            this.fEventDelayMultiplier.Value = new decimal(new int[] {
            100,
            0,
            0,
            131072});
            this.fEventDelayMultiplier.ValueChanged += new System.EventHandler(this.fEventDelayMultiplier_ValueChanged);
            // 
            // fMacroRepeat
            // 
            this.fMacroRepeat.AutoSize = true;
            this.fMacroRepeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fMacroRepeat.Location = new System.Drawing.Point(252, 148);
            this.fMacroRepeat.Name = "fMacroRepeat";
            this.fMacroRepeat.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.fMacroRepeat.Size = new System.Drawing.Size(226, 19);
            this.fMacroRepeat.TabIndex = 21;
            this.fMacroRepeat.Text = "Циклическое повторение макроса";
            this.fMacroRepeat.UseVisualStyleBackColor = true;
            this.fMacroRepeat.CheckedChanged += new System.EventHandler(this.fMacroRepeat_CheckedChanged);
            // 
            // fActivationMethod
            // 
            this.fActivationMethod.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fActivationMethod.FormattingEnabled = true;
            this.fActivationMethod.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.fActivationMethod.Items.AddRange(new object[] {
            "Удержание",
            "Переключение"});
            this.fActivationMethod.Location = new System.Drawing.Point(371, 119);
            this.fActivationMethod.Name = "fActivationMethod";
            this.fActivationMethod.Size = new System.Drawing.Size(109, 23);
            this.fActivationMethod.TabIndex = 20;
            this.fActivationMethod.SelectedValueChanged += new System.EventHandler(this.fActivationMethod_SelectedValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(249, 122);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 15);
            this.label6.TabIndex = 19;
            this.label6.Text = "Способ активации:";
            // 
            // bMacroActivationKeySet
            // 
            this.bMacroActivationKeySet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bMacroActivationKeySet.Location = new System.Drawing.Point(444, 90);
            this.bMacroActivationKeySet.Name = "bMacroActivationKeySet";
            this.bMacroActivationKeySet.Size = new System.Drawing.Size(75, 23);
            this.bMacroActivationKeySet.TabIndex = 18;
            this.bMacroActivationKeySet.Text = "Изменить";
            this.bMacroActivationKeySet.UseVisualStyleBackColor = true;
            this.bMacroActivationKeySet.Click += new System.EventHandler(this.bMacroActivationKeySet_Click);
            this.bMacroActivationKeySet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.bMacroActivationKeySet_KeyDownEvent);
            // 
            // fMacroActivationKey
            // 
            this.fMacroActivationKey.AutoSize = true;
            this.fMacroActivationKey.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.fMacroActivationKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fMacroActivationKey.Location = new System.Drawing.Point(371, 91);
            this.fMacroActivationKey.MaximumSize = new System.Drawing.Size(70, 20);
            this.fMacroActivationKey.MinimumSize = new System.Drawing.Size(20, 20);
            this.fMacroActivationKey.Name = "fMacroActivationKey";
            this.fMacroActivationKey.Size = new System.Drawing.Size(70, 20);
            this.fMacroActivationKey.TabIndex = 17;
            this.fMacroActivationKey.Text = "OemMinus";
            this.fMacroActivationKey.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(249, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 15);
            this.label4.TabIndex = 16;
            this.label4.Text = "Кнопка актавации:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(249, 227);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 15);
            this.label3.TabIndex = 10;
            this.label3.Text = "Задержка:";
            // 
            // fMacroName
            // 
            this.fMacroName.Location = new System.Drawing.Point(371, 63);
            this.fMacroName.Name = "fMacroName";
            this.fMacroName.Size = new System.Drawing.Size(148, 20);
            this.fMacroName.TabIndex = 11;
            this.fMacroName.TextChanged += new System.EventHandler(this.fMacroName_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(249, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 15);
            this.label2.TabIndex = 10;
            this.label2.Text = "Название Макроса:";
            // 
            // bNewMacro
            // 
            this.bNewMacro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bNewMacro.Location = new System.Drawing.Point(18, 25);
            this.bNewMacro.Name = "bNewMacro";
            this.bNewMacro.Size = new System.Drawing.Size(119, 23);
            this.bNewMacro.TabIndex = 15;
            this.bNewMacro.Text = "Создать Макрос";
            this.bNewMacro.UseVisualStyleBackColor = true;
            this.bNewMacro.Click += new System.EventHandler(this.bNewMacro_Click);
            // 
            // bDeleteMacro
            // 
            this.bDeleteMacro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bDeleteMacro.Location = new System.Drawing.Point(143, 25);
            this.bDeleteMacro.Name = "bDeleteMacro";
            this.bDeleteMacro.Size = new System.Drawing.Size(119, 23);
            this.bDeleteMacro.TabIndex = 10;
            this.bDeleteMacro.Text = "Удалить Макрос";
            this.bDeleteMacro.UseVisualStyleBackColor = true;
            this.bDeleteMacro.Click += new System.EventHandler(this.bDeleteMacro_Click);
            // 
            // fExtendedButtonSelection
            // 
            this.fExtendedButtonSelection.AutoSize = true;
            this.fExtendedButtonSelection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fExtendedButtonSelection.Location = new System.Drawing.Point(281, 15);
            this.fExtendedButtonSelection.Name = "fExtendedButtonSelection";
            this.fExtendedButtonSelection.Size = new System.Drawing.Size(167, 19);
            this.fExtendedButtonSelection.TabIndex = 16;
            this.fExtendedButtonSelection.Text = "Extended button selection";
            this.fExtendedButtonSelection.UseVisualStyleBackColor = true;
            // 
            // statusStrip_mainMessage
            // 
            this.statusStrip_mainMessage.Name = "statusStrip_mainMessage";
            this.statusStrip_mainMessage.Size = new System.Drawing.Size(69, 17);
            this.statusStrip_mainMessage.Text = "<Message>";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusStrip_Debug,
            this.statusStrip_mainMessage});
            this.statusStrip.Location = new System.Drawing.Point(0, 471);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.statusStrip.Size = new System.Drawing.Size(721, 22);
            this.statusStrip.TabIndex = 10;
            this.statusStrip.Text = "statusStrip";
            // 
            // statusStrip_Debug
            // 
            this.statusStrip_Debug.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.statusStrip_Debug.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.debug_TFexecTime,
            this.debug_enabledIndexListAndSignals,
            this.debug_macroEnabled_indexList,
            this.debug_macroCollection_IndexList,
            this.debug_macroData,
            this.debug_mainMacroCollection});
            this.statusStrip_Debug.Image = ((System.Drawing.Image)(resources.GetObject("statusStrip_Debug.Image")));
            this.statusStrip_Debug.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.statusStrip_Debug.Name = "statusStrip_Debug";
            this.statusStrip_Debug.Size = new System.Drawing.Size(71, 20);
            this.statusStrip_Debug.Text = "<Debug>";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.saveToolStripMenuItem.Text = "<Save>";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // debug_TFexecTime
            // 
            this.debug_TFexecTime.Name = "debug_TFexecTime";
            this.debug_TFexecTime.Size = new System.Drawing.Size(225, 22);
            this.debug_TFexecTime.Text = "TF, HK, HM execution time";
            this.debug_TFexecTime.Click += new System.EventHandler(this.debug_TFexecTime_Click);
            // 
            // debug_enabledIndexListAndSignals
            // 
            this.debug_enabledIndexListAndSignals.Name = "debug_enabledIndexListAndSignals";
            this.debug_enabledIndexListAndSignals.Size = new System.Drawing.Size(225, 22);
            this.debug_enabledIndexListAndSignals.Text = "enabledIndexList and Signals";
            this.debug_enabledIndexListAndSignals.Click += new System.EventHandler(this.debug_enabledIndexListAndSignals_Click);
            // 
            // debug_macroEnabled_indexList
            // 
            this.debug_macroEnabled_indexList.Name = "debug_macroEnabled_indexList";
            this.debug_macroEnabled_indexList.Size = new System.Drawing.Size(225, 22);
            this.debug_macroEnabled_indexList.Text = "macroEnabled_IndexList";
            this.debug_macroEnabled_indexList.Click += new System.EventHandler(this.debug_macroEnabled_indexList_Click);
            // 
            // debug_macroCollection_IndexList
            // 
            this.debug_macroCollection_IndexList.Name = "debug_macroCollection_IndexList";
            this.debug_macroCollection_IndexList.Size = new System.Drawing.Size(225, 22);
            this.debug_macroCollection_IndexList.Text = "macroCollection_IndexList";
            this.debug_macroCollection_IndexList.Click += new System.EventHandler(this.debug_macroCollection_IndexList_Click);
            // 
            // debug_macroData
            // 
            this.debug_macroData.Name = "debug_macroData";
            this.debug_macroData.Size = new System.Drawing.Size(225, 22);
            this.debug_macroData.Text = "Selected Macro.data";
            this.debug_macroData.Click += new System.EventHandler(this.debug_macroData_Click);
            // 
            // debug_mainMacroCollection
            // 
            this.debug_mainMacroCollection.Name = "debug_mainMacroCollection";
            this.debug_mainMacroCollection.Size = new System.Drawing.Size(225, 22);
            this.debug_mainMacroCollection.Text = "mainMacroCollection";
            this.debug_mainMacroCollection.Click += new System.EventHandler(this.debug_mainMacroCollection_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.fDisableMainMouseBtn);
            this.groupBox1.Controls.Add(this.f_mwTimeout);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.fUseVirtualKeyCodes);
            this.groupBox1.Controls.Add(this.bNewMacro);
            this.groupBox1.Controls.Add(this.fExtendedButtonSelection);
            this.groupBox1.Controls.Add(this.bDeleteMacro);
            this.groupBox1.Location = new System.Drawing.Point(12, 394);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(698, 69);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Дополнительно";
            // 
            // fDisableMainMouseBtn
            // 
            this.fDisableMainMouseBtn.AutoSize = true;
            this.fDisableMainMouseBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fDisableMainMouseBtn.Location = new System.Drawing.Point(467, 15);
            this.fDisableMainMouseBtn.Name = "fDisableMainMouseBtn";
            this.fDisableMainMouseBtn.Size = new System.Drawing.Size(182, 19);
            this.fDisableMainMouseBtn.TabIndex = 20;
            this.fDisableMainMouseBtn.Text = "Disable Mouse L/R handling";
            this.fDisableMainMouseBtn.UseVisualStyleBackColor = true;
            this.fDisableMainMouseBtn.CheckedChanged += new System.EventHandler(this.fDisableMainMouseBtn_CheckedChanged);
            // 
            // f_mwTimeout
            // 
            this.f_mwTimeout.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.f_mwTimeout.Location = new System.Drawing.Point(593, 40);
            this.f_mwTimeout.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.f_mwTimeout.Name = "f_mwTimeout";
            this.f_mwTimeout.Size = new System.Drawing.Size(49, 21);
            this.f_mwTimeout.TabIndex = 19;
            this.f_mwTimeout.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.f_mwTimeout.ValueChanged += new System.EventHandler(this.f_mwTimeout_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(464, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(130, 15);
            this.label5.TabIndex = 18;
            this.label5.Text = "Mouse Wheel timeout:";
            // 
            // fUseVirtualKeyCodes
            // 
            this.fUseVirtualKeyCodes.AutoSize = true;
            this.fUseVirtualKeyCodes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fUseVirtualKeyCodes.Location = new System.Drawing.Point(281, 40);
            this.fUseVirtualKeyCodes.Name = "fUseVirtualKeyCodes";
            this.fUseVirtualKeyCodes.Size = new System.Drawing.Size(140, 19);
            this.fUseVirtualKeyCodes.TabIndex = 17;
            this.fUseVirtualKeyCodes.Text = "Use virtual key codes";
            this.fUseVirtualKeyCodes.UseVisualStyleBackColor = true;
            this.fUseVirtualKeyCodes.CheckedChanged += new System.EventHandler(this.fUseVirtualKeyCodes_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.fAddEventMode);
            this.groupBox2.Controls.Add(this.bMacroDataDelete);
            this.groupBox2.Controls.Add(this.bAddEvent);
            this.groupBox2.Controls.Add(this.bMacroDataClear);
            this.groupBox2.Controls.Add(this.fAddEventSel);
            this.groupBox2.Location = new System.Drawing.Point(252, 264);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(263, 90);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            // 
            // MainWindowCtr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 493);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.group_macroConfig);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.group_macroCollection);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainWindowCtr";
            this.Text = "wxMacro";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindowCtr_FormClosing);
            this.group_macroCollection.ResumeLayout(false);
            this.group_macroCollection.PerformLayout();
            this.group_macroConfig.ResumeLayout(false);
            this.group_macroConfig.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fEventDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fEventDelayMultiplier)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.f_mwTimeout)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox macroCollection;
        private System.Windows.Forms.ListBox macroEnabled;
        private System.Windows.Forms.ListBox macroData;
        private System.Windows.Forms.Button bMacroDataDelete;
        private System.Windows.Forms.Button bStartMacroRecord;
        private System.Windows.Forms.Button bStopMacroRecord;
        private System.Windows.Forms.Button bMacroEnable;
        private System.Windows.Forms.Button bMacroDisable;
        private System.Windows.Forms.GroupBox group_macroCollection;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox group_macroConfig;
        private System.Windows.Forms.TextBox fMacroName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bDeleteMacro;
        private System.Windows.Forms.Button bMacroDataClear;
        private System.Windows.Forms.Button bNewMacro;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label fMacroActivationKey;
        private System.Windows.Forms.ComboBox fActivationMethod;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button bMacroActivationKeySet;
        private System.Windows.Forms.NumericUpDown fEventDelay;
        private System.Windows.Forms.CheckBox fExtendedButtonSelection;
        private System.Windows.Forms.ToolStripStatusLabel statusStrip_mainMessage;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox fMacroRepeat;
        private System.Windows.Forms.ToolStripDropDownButton statusStrip_Debug;
        private System.Windows.Forms.ToolStripMenuItem debug_mainMacroCollection;
        private System.Windows.Forms.ToolStripMenuItem debug_macroData;
        private System.Windows.Forms.ToolStripMenuItem debug_macroEnabled_indexList;
        private System.Windows.Forms.ToolStripMenuItem debug_macroCollection_IndexList;
        private System.Windows.Forms.ToolStripMenuItem debug_enabledIndexListAndSignals;
        private System.Windows.Forms.ToolStripMenuItem debug_TFexecTime;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown fEventDelayMultiplier;
        private System.Windows.Forms.CheckBox fUseVirtualKeyCodes;
        private System.Windows.Forms.CheckBox fPreventTermination;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ComboBox fAddEventSel;
        private System.Windows.Forms.Button bAddEvent;
        private System.Windows.Forms.ComboBox fAddEventMode;
        private System.Windows.Forms.NumericUpDown f_mwTimeout;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox fDisableMainMouseBtn;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}

