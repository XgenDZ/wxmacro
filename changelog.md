﻿---------- v0.7.1rc [global update]

[bug fixed] bAddEvent (new): macroData.Items.Add() --> macroData_AddItem()
[bug fixed] useVirtualKeys invert issue
[bug fixed] #AE0D & #C38D
[bug fixed] #823B

[changed] macroData Box Native Update
[changed] working mouse support
[changed] key event porc moved
[changed] new file saving
[changed] macroData box size

[changed] Код приведен в порядок

[added] disable L&R Mouse Buttons handling
[added] new key handling (full)
[added] IO exception handling
[added] close save

---------- v0.6.9d

[cross] MouseKeyHook lib (https://github.com/gmamaladze/globalmousekeyhook)
[fixed] act key: mouse keys working now

[fixed] key emulation handler: rmb support

---------- v0.6.8d

[changed] selectedIndex delete fix

[added] add macro event logic
[added] key select dialog full logic
[added] add act key select logic (ex)
[added] key emulation handler: rmb support

---------- v0.6.7d

[bug fixed] add macro: fEventDelay_ValueChanged(): macroDataSelectedIndex out of range

[changed] gebug funcs restore

[added] protection from the sticky keys
[added] virtual key codes trigger
[added] File I/O: saving functionality

---------- v0.6.6d

[bug fixed] macroData DeleteEvent last item pointer out of range

[added] base mouse support
[added] macroData DeleteEvent button functionality
[added] macroData Clear button functionality
[added] macroData DelayMultiplier functionality

---------- v0.6.5d [global update]

[changed] New TF logic
[changed] New WF logic
[changed] New Data logic
[changed] extended button recorging 2

[changed] Код приведен в порядок, убран говнокод.

[added] extended button recording

---------- v0.6.4d

[added] mouse support
[added] terminate control

---------- v0.6.3d

[changed] Problem Solved: ACTM_SWITCH: reactivation and strange behavior
	Моментальная переактивация при методе ACTM_SWITCH, странное поведение кода. Причина:
	слишком быстрое обновление цикла, на повторных проходах кнопка не успевает отжаться
	и проверка на нажатие вновь срабатывает. Решение: моментальное программное отжатие
	сразу после выполнения проверки.
[changed] Macro & MacroDataAtom & Key

[added] kbd: protection from sticking (key up check)
[added] hardware scan codes: full keyboard support
[added] automatic translation to the scan code, all keyboards support
[added] UseVirtualKeyCodes trigger

---------- v0.6.2d

[changed] Problem Solved: first key check fail
	Первый вызов GetAsyncKeyState в программе всегда возвращает 0 (некорректное значение).
	Это приводило к неправильной фиксации первого нажатия, но с обновлением data_counter.
	Решение: добавлены мусорные вызовы GetAsyncKeyState перед основными.
[changed] Problem Solved: TF() exception: unexpected data changes: added en/dis signals
[changed] Problem Solved: macro repeating: out of range index issue
[changed] Problem Solved: macro repeating: reactivation issue: finished not updating

[added] Second thread
[added] Multihandling
[added] extended kbd handling
[added] debug msg: enabledIndexList and Signals
[added] debig msg: tf_timer (exec time)

---------- v0.6.1r

[changed] macroEnabled_indexList -> public
[changed] Macro class

[added] kbd event base functionality (only 1 macro)
[added] threading support

---------- v0.5.5d

[changed] Problem Solved: fMacroAtomDelay_ValueChanged(): List<MacroDataAtom>.this[int]
	Для изменения значения списка используется промежуточная var-переменная.
[changed] macroData logic + custom focus var
[changed] bStartMacroRecord_KeyDown(): added macroData_Update #TODO Remove, add flag var
[changed] MacroDataAtom struct: garbage deleted
[changed] Macro class: garbade deleted

[added] macroData_selectedIndex
[added] fMacroAtomDelay logic

---------- v0.5.4d

[bug fixed] Epic CopyPase Bug: macroEnabled_Update()
	При вызове bMacroEnable_Click() элемент не удалялся из macroCollection
	и не появлялся в macroEnabled. При этом, macroCollection_indexList и
	macroEnabled_indexList генерировались правильно. Плюс многократный вызов
	macroCollection_Update(). Причина: тупая копиписта macroEnabled_Update()
	с macroCollection_Update(): одна переменная не была изменена => при
	при вызове macroEnabled_Update() элемент добавлялся в macroCollestion,
	а не в macroEnabled.

[changed] macroData_SelectedIndexChanged() & fMacroAtomDelay_ValueChanged() commented

[added] Window: Double Buffering
[added] fMacroData logic: macroData_SelectedIndexChanged()

[added] MacroDataAtom Garbage functions: Set_dur() & Ser_del() [НЕ ПОМОГЛО]
	Ошибка	CS1612	Не удалось изменить возвращаемое значение "List<MacroDataAtom>.this[int]", т. к. оно не является переменной. #TODO: решить проблему, убрать мусорные функции


---------- v0.5.3d

[bug fixed] bMacroActivationKeySet_KeyDownEvent() issue
	При вызове group_macroConfig_FillData(), поле fMacroActivationKey
	сохраняло свое старое значение. Причина: отсутствие записи в поле
	Macro::actKeyStr в ф-ии bMacroActivationKeySet_KeyDownEvent().

[changed] Macro::Macro(string name): new defaults
[changed] MacroDataAtom struct: new constructor x2
[changed] Key struct: new constructor
[changed] debug_macroData_Click(): compatibility with WFApp.Key
[changed] group_macroConfig_FillData(): added macroData_Update()
[changed] Form Size: macroData Size
	Не влезал OemMinus. Возможно стоит перенести duration в свое поле.

[added] Recording Macro::data base functionality
	bStartMacroRecord_KeyDown() & bStartMacroRecord_KeyUp()
[added] macroData_IndexList[]
[added] macroData_Update()

---------- v0.5.2

Изициализация git репозиотрия. Начало отслеживания.